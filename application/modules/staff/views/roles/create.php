<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Elements</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="<?= base_url().'staff/staff_role/save'?>" method="post">
							<fieldset>
							  <div class="control-group">
									<label class="control-label" for="focusedInput">Role</label>
									<div class="controls">
										<input class="input-xlarge focused" name="role" id="focusedInput" type="text" placeholder="Enter Role">
									</div>
							  </div>
							  <div class="control-group">
									<label class="control-label" for="focusedInput">Description</label>
									<div class="controls">
										<input class="input-xlarge focused" name="description" id="focusedInput" type="text" placeholder="Enter Description">
									</div>
							  </div>
							  <div class="form-actions">
								<button type="submit" class="btn btn-primary">Save changes</button>
								<!-- <button class="btn">Cancel</button> -->
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

    
<?php temp_view('roles/list',$staff_roles);?>