
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>List of Manufacturas/Make</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Role</th>
								  <th>Description</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
                <?php foreach($staff_roles as $staff_role):?>
							<tr>
								<td><?= $staff_role->role;?></td>
								<td><?= $staff_role->description;?></td>
								<td class="center span6">
									<a class="btn btn-success" href="<?= base_url().'staff/staff_role/edit/'.$staff_role->id?>">
										Edit  
									</a>
									<a class="btn btn-danger" href="<?= base_url().'staff/staff_role/delete/'.$staff_role->id?>">
										Delete 
									</a>
								</td>
              </tr>
              <?php endforeach;?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

