<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Staff_role extends MX_Controller
{

    protected $model;
    private $url_segments = 'staff/staff_role/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('staff_roles');
  }

  public function staff_roles()
  {
        
        $data['headline'] = "staff_roles";
        $data['staff_roles'] = $this->model->get('role')->result();
        load_view($data);
    }

    public function staff_role($update_id)
    {
        $staff_role = $this->model->get('role')->row();
        $data['headline'] = "Edit $staff_role->role";
        $data['staff_roles'] = $staff_role;
        load_view($data);

    }

     public function add(){
          $data['headline'] = "Add staff_role";
          $data['message'] = get_flashdata();

          $data['staff_roles'] = $this->model->get('role')->result();
          $data['view_file'] = 'roles/create';
          load_admin($data);
      }

    public function save()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('role', 'Role', 'trim|required');
        
        if ($this->form_validation->run()) {
            $data['role'] = $this->input->post('role');
            $data['description'] = $this->input->post('description');
            
            if (!$this->model->_insert($data)) {

                $message = "Database operation failed";
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'create');
            }else{
                
                $message = "Success";
                 set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = response_message(validation_errors());
             set_flashdata($message);
                redirect($this->url_segments.'add');
        }

    }

    public function edit($update_id){
        $staff_role = $this->model->get_where($update_id)->row();
        $data['role'] = $staff_role->role;
        $data['id'] = $update_id;

        $data['staff_roles'] = $this->model->get('role')->result();
        $data['view_file'] = 'role/edit';
        load_view($data);
    }

    public function update($update_id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role', 'Name', 'trim|required');
        if ($this->form_validation->run()) {
            $data['role'] = $this->input->post('role');
            $data['description'] = $this->input->post('description');
            if (!$this->model->_update($update_id,$data)) {

                $message = "Database operation failed";
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                
                $message = "Success";
                 set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = response_message(validation_errors());
             set_flashdata($message);
                redirect($this->url_segments.'add');
        }
    }

    public function delete($id)
    {
       
        if($this->model->_delete($id)){
              $message = 'delete successful';
              set_flashdata($message);
                  redirect($this->url_segments.'add');
          }
    }

}
