<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Staff extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('users');
    }

    public function staffs()
      {
         if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['staffs'] = $this->model->get('first_name')->result();
          $data['view_module'] = 'staff';
          load_view($data);
      }

      public function staff($update_id)
      {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['staff'] = $this->model->get($update_id)->row_array();
          load_view($data);
      }


      public function _get_staff_roles(){
        $color = new main_model;
        $color->setTable('staff_roles');
        return $color->get('role')->result();
    }

      public function add(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->model('auth/mdl_auth');

          $data['headline'] = "Add Staff";
          $data['staffs'] = $this->mdl_auth->get_accounts_by_group('staffs');
          $data['staff_roles'] = $this->_get_staff_roles();
          $data['message'] = get_flashdata();
          $data['view_file'] = 'create';
          load_admin($data);
      }

      public function save()
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');
          $_POST = form_data();
          $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
         
            if ($this->form_validation->run()) {
                $data = $this->_get_form_data();
              if (!$this->model->_insert($data)) {

                  $message = alert_message("Database operation failed", false);
                //   print_r($message);die();
                  set_flashdata($message);
                  redirect('staff/add');
              }else{
                  $this->load->model('auth/mdl_auth');
                  
                  $staff_group_id = $this->mdl_auth->get_group_id_by_name('staffs')->id;
                //   pp($staffs_group_id);

                  $staff_id = $this->model->get_max();

                  $this->ion_auth->add_to_group($staff_group_id, $staff_id);
                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect('staff/add');
                }
                
            } else {
                $data['message'] = validation_errors('<div class="alert alert-danger">','</div>');
                
                redirect('staff/add');
                
          }

      }

      public function edit(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $staff = $this->model->get($update_id)->row();

        $data = $this->_get_db_data($staff);

        $data['message'] = get_flashdata();
        $data['headline'] = "Edit $staff->name";
        $data['view_file'] = 'edit';
        load_view($data);
      }

      public function update($update_id)
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');
          $this->form_validation->set_rules('name', 'Name', 'trim|required');

          if ($this->form_validation->run()) {
                $data = $this->_get_form_data();
              if (!$this->model->_update($update_id,$data)) {

                  $message = alert_message("Database operation failed", false);
                //   print_r($message);die();
                  set_flashdata($message);
                  redirect('staff/add');
              }else{

                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect('staff/add');
                }
                
            } else {
                $data['message'] = validation_errors('<div class="alert alert-danger">', '</div>');
                
                redirect('staff/add');
                
          }
      }

      public function delete($id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          if($this->model->_delete($id)){
              $message = alert_message('delete successful', false);
              set_flashdata($message);
              redirect('staff/staffs');
          }
      }

      function _get_db_data($staff){
        $data['username'] = $staff->username;
        $data['first_name'] = $staff->first_name;
        $data['last_name'] = $staff->last_name;
        $data['phone'] = $staff->phone;
        $data['email'] = $staff->email;
        $data['company'] = $staff->company;
        $data['role'] = $staff->role;
        return $data;
      }

      function _get_form_data($update_id = null){
        $id_count = $this->model->get_max() + 1;
        $this->load->model('ion_auth_model','iam');
        $data['code'] = uniq_id();
        $data['username'] = 'staff_'.$id_count;
        $data['role'] = input_post('role');
        $data['password'] = $this->iam->hash_password($data['username']);
        $data['first_name'] = input_post('first_name');
        $data['last_name'] = input_post('last_name');
        $data['phone'] = input_post('phone');
        $data['email'] = input_post('email');
        $data['date_added'] = time();
        $data['ip_address'] = $this->input->ip_address();
        
        if(isset($update_id)){
            unset($data['username']);
            unset($data['password']);
            unset($data['code']);
            unset($data['date_added']);
        }
        
        return $data;
      }
}
