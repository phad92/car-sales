<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MX_Controller
{

function __construct() {
parent::__construct();
}
    public function index(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['view_file'] = 'index';
        load_admin($data);
    }

    public function home(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

        $data['view_file'] = 'index';
        load_admin($data);
    }
}