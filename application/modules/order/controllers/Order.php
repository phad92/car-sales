<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Order extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('cs_order');
    }

    public function fetch_foreign_data($table,$column){
        $model = new main_model;
        $model->setTable($table);
        return $model->get($column)->result();
    }

    function check_approval($order_id = ''){
        $this->load->config('app_config');
        $track_order = $this->config->item('order_tracking');
        pp($track_order);
        $modelInstance = new main_model;
        $modelInstance->setTable('cs_track_order');
        $track_order = $modelInstance->get_where_custom('cs_order',$order_id);
        if($track_order->num_rows() < 1){
            $text = 'not approved';
        }else{
            $current_state = $track_order->current_state;
            if(!isset($current_state) || $current_state < 1){
                
            }else{
    
            }
        }
        
    }

    public function orders()
    {
        $data['headline'] = "Orders";
        $data['message'] = get_flashdata();
        $data['view_file'] = "list";
        $data['orders'] = $this->model->get('date_added')->result();
        load_admin($data);
    }

    public function order($update_id)
    {
        $data['order'] = $this->model->get_where($update_id)->row_array();
        $data['id'] = $update_id;
        $this->load->model('mdl_order','pm');
        
        $data['orders'] = $this->pm->_order_details();
        $data['message'] = get_flashdata();
        $data['headline'] = 'Order Detail';
        $data['view_file'] = 'detail';
        load_admin($data);
    }

    public function create(){
        $this->load->model('mdl_order');
        $this->load->model('auth/mdl_auth');
        $data['orders'] = $this->model->get('code')->result();
        $data['products'] = $this->fetch_foreign_data('cs_product','make');
        $data['staffs'] = $this->mdl_auth->get_accounts_by_group('staffs');
        $data['clients'] = $this->mdl_auth->get_accounts_by_group('clients');
        $data['message'] = get_flashdata();
        $data['load_icheck'] = true;
        $data['headline'] = 'Create Order';
        $data['view_file'] = 'create';
        load_admin($data);
    }

      public function save()
      {
          $this->load->library('form_validation');

          $this->form_validation->set_rules('product', 'Product', 'trim|required');
        
        if ($this->form_validation->run()) {
            $data = $this->_get_form_data();
            // pp($data);
            if (!$this->model->_insert($data)) {
                  $message = alert_message("Database operation failed", false);
                  set_flashdata($message);
                  redirect('order/create');
              }else{
                $order_id = $this->model->get_max();
                if(isset($data['approved'])){
                    $this->load->module('track_order');
                    $this->track_order->start_tracking($order_id);
                }
                   
                if(input_post('proceedToPayment')){
                    $last_order = $this->model->get_max();
                    $redirect = 'payment/make_payment?order='.$last_order;
                    redirect($redirect);
                }
                $message = alert_message("Success", true);
                set_flashdata($message);
                redirect('order/create');
            }
          } else {
              $message = validation_errors('<div class="alert alert-danger">', '</div>');
              set_flashdata($message);
                redirect('order/create');
          }

      }

      public function edit($update_id){
          $this->load->model('mdl_order');
          $order = $this->model->get_where($update_id)->row();
          
          $data = $this->_get_db_data($order);
          $data['orders'] = $this->model->get('code')->result();
          $data['products'] = foreign_result('cs_product','make');
          
          $data['staffs'] = fetch_accounts('staffs');
          $data['clients'] = fetch_accounts('clients');
          $data['id'] = $update_id;
          $data['load_icheck'] = true;
          $data['headline'] = "Edit Order $order->code";
          $data['message'] = get_flashdata();
          $data['view_file'] = 'edit';
          load_admin($data);
      }

      public function update($update_id)
      {
          $this->load->library('form_validation');

          $this->form_validation->set_rules('product', 'Product', 'trim|required');
          
          $data = $this->_get_form_data();

        if ($this->form_validation->run()) {
            if (!$this->model->_update($update_id,$data)) {
                  $message = alert_message("Database operation failed", false);
                  set_flashdata($message);
                  redirect('order/edit/'.$update_id);
              }else{
                  $this->load->module('track_order');
                if(!$this->track_order->order_exists($update_id)){
                    if(isset($data['approved'])){
                        $this->track_order->start_tracking($update_id);
                    }
                }
                   
                if(input_post('proceedToPayment')){
                    $last_order = $this->model->get_max();
                    $redirect = 'payment/make_payment?order='.$last_order;
                    redirect($redirect);
                }

                $message = alert_message("Success", true);
                set_flashdata($message);
                redirect('order/edit/'.$update_id);
            }
          } else {
              $message = validation_errors('<div class="alert alert-danger">', '</div>');
              set_flashdata($message);
                redirect('order/edit/'.$update_id);
          }

      }

      

      public function delete($id){
       
          if($this->model->_delete($id)){
              $message = alert_message('delete successful', true);
              set_flashdata($message);
                  redirect('/order/orders');
          }
      }


    function _get_db_data($order){
        $data['id'] = $order->id;
        $data['code'] = $order->code;
        $data['product'] = $order->product;
        $data['quantity'] = $order->quantity;
        $data['client'] = $order->client;
        $data['staff'] = $order->staff;
        $data['status'] = $order->status;
        $data['approved'] = $order->approved;
        $data['approved_by'] = $order->approved_by;
        $data['comment'] = $order->comment;
        // pp($data);
        return $data;
      }

      function _get_form_data($update_id = null){
        // added the staff using session
        $data['code'] = uniq_id(6);
        $data['product'] = input_post('product');
        $data['quantity'] = input_post('quantity');
        $data['client'] = input_post('client');
        $data['staff'] = input_post('staff');
        $data['status'] = input_post('status');
        $data['approved'] = input_post('approved');
        $data['approved_by'] = input_post('approved_by');
        $data['comment'] = input_post('comment');
        $data['date_added'] = time();

        if(!$data['status']){
            $data['status'] = 'pending';
            $data['approved'] = 0;
        }
        if(isset($update_id)){
            unset($data['code']);
            unset($data['uniq_id']);
            unset($data['date_added']);
        }

        return $data;
      }


  
}
