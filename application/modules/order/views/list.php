<div class="row"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title">
    <h2>List of Existing Services</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Settings 1</a>
          </li>
          <li><a href="#">Settings 2</a>
          </li>
        </ul>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <!-- <p class="text-muted font-13 m-b-30">
      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
    </p> -->
				<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                    <thead>
                      <tr role="row">
						<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 10px;">#</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Code</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Product</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 20px;">Quantity</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 90px;">Shipping Cost</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Client</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Status</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Approved</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Current Tracking State</th>
						<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 250px;">Action</th>
					</tr>
                    </thead>
                    <tbody>
				  	<?php $count = 0; foreach($orders as $order):
					$current_state_index = get_current_tracking_status($order->id);
					$product = foreign_row('cs_product',$order->product); $count++;
					$model = foreign_row('cs_model',$product->model);
          $make = foreign_row('cs_make',$model->make);
					?>
							<tr>
								<td><?= $count;?></td>
								<td><?= $order->code;?></td>
								<td><?= $make->name .' - '. $model->name;?></td>
								<td><?= $order->quantity;?></td>
								<td><?= 'Ghc'. $product->shipping_cost;?></td>
								<td><?= account_name('clients',$order->client);?></td>
								<td><?= $order->status;?></td>
								<td><?= ($order->approved) ? 'Yes' : 'No';?></td>
								<td><?= readable_current_state($current_state_index);?></td>
							<td>
								<a class="btn btn-xs btn-info" href="<?= base_url().'track_order/check_state/'.$order->id?>">
										Track
									</a>
									<a class="btn btn-xs btn-success" href="<?= base_url().'order/edit/'.$order->id?>">
										Edit  
									</a>
									<a class="btn btn-xs btn-danger" href="<?= base_url().'order/delete/'.$order->id?>">
										Delete 
									</a>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
                  </table></div></div>
                </div>
              </div>
            </div>





