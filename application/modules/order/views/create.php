

<div class="row">
  <div class="x_panel">
                  <div class="x_title">
                    <h2><?= $headline?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form action="<?= base_url().'order/save'?>" method="post" class="form-horizontal form-label-left">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product">Select Product <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select name="product" id="product" class="form-control">
                                <option value="">Select Product</option>
                                <?php foreach($products as $product):
                                  $model = foreign_row('cs_model',$product->model);
                                  $make = foreign_row('cs_make',$model->make);
                                  ?>
                                  <option value="<?= $product->id?>"><?= ucwords($make->name.' - '.$model->name)?></option>
                                <?php endforeach?>
                              </select>
                            </div>
                        </div>

                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Quantity <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="quantity" name="quantity" required="required" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>

                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="client">Select Client <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="client" id="client" class="form-control">
                              <option value="">Select Client</option>
                              <?php foreach($clients as $client):?>
                                <option value="<?= $client->id?>"><?= $client->first_name . ' ' . $client->last_name?></option>
                              <?php endforeach?>
                            </select>
                          </div>
                        </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Staff <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="staff" id="staff" class="form-control">
                            <option value="">Select Staff</option>
                            <?php foreach($staffs as $staff):?>
                              <option value="<?= $staff->id?>"><?= $staff->first_name . ' ' . $staff->last_name?></option>
                            <?php endforeach?>
                          </select>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">Order Status
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="checkbox">
                            <label class="">
                                <div class="icheckbox_flat-green checked" style="position: relative;">
                                    <input type="checkbox" class="flat" id="status" name="status" value="confirmed" style="position: absolute; opacity: 0;">
                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div> Confirm Order
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">Approve Order
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="checkbox">
                            <label class="">
                                <div class="icheckbox_flat-green checked" style="position: relative;">
                                    <input type="checkbox" class="flat" id="approved" name="approved" value="1" style="position: absolute; opacity: 0;">
                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div> Approve Order
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">Payment
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <div class="checkbox">
                            <label class="">
                                <div class="icheckbox_flat-green checked" style="position: relative;">
                                    <input type="checkbox" class="flat" id="proceedToPayment" name="proceedToPayment" value="1" style="position: absolute; opacity: 0;">
                                    <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div> Proceed to Payment ?
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" name="comment" rows="3"></textarea>
                        </div>
                      </div>

                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
</div>
