<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_Auth extends CI_Model {

function __construct() {
    parent::__construct();
}

function get_group_id_by_name($group_name){
    $this->db->select('id');
    $this->db->from('groups');
    $this->db->where('name',$group_name);
    return $this->db->get()->row();
}

function get_accounts_by_group($group_name){
    $mysql_query = "users.id,users.first_name,
        users.last_name,
        users.phone,
        users.email,
        users.physical_address,
        users.city,
        users.type_of_id,
        users.id_number,
        users.tin,
        groups.`name` AS `group`";
    $this->db->select($mysql_query);
    $this->db->from('users');
    $this->db->join('users_groups','users_groups.user_id = users.id','inner');
    $this->db->join('groups','users_groups.group_id = groups.id','inner');
    $this->db->where('groups.name',$group_name);
    return $this->db->get()->result();
}


function get_account_by_group($group_name,$id){
    $mysql_query = "users.id,users.first_name,
        users.last_name,
        users.phone,
        users.email,
        users.physical_address,
        users.city,
        users.type_of_id,
        users.id_number,
        users.tin,
        groups.`name` AS `group`";
    $this->db->select($mysql_query);
    $this->db->from('users');
    $this->db->join('users_groups','users_groups.user_id = users.id','inner');
    $this->db->join('groups','users_groups.group_id = groups.id','inner');
    $this->db->where('groups.name',$group_name);
    $this->db->where('users.id',$id);
    
    return $this->db->get()->row();
}

}