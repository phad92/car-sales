<div class="login_wrapper">
      <div id="register" class="animate form">
          <?= ($message) ?? $message?>
          <section class="login_content">
            <form action="<?= base_url().'auth/register'?>" method="post">
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="" />
              </div>

              <div>
                <input type="text" class="form-control" name="last_name" placeholder="Last Name" required="" />
              </div>

              <div>
                <input type="email" class="form-control" name="email" placeholder="Email" required="" />
              </div>

              <div>
                <input type="text" class="form-control" name="company" placeholder="Company" />
              </div>
              <div>
                <input type="text" class="form-control" name="phone" placeholder="Phone" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password_confirm" placeholder="Confirm Password" />
              </div>
              <div>
                <input type="submit" class="btn btn-default submit" name="submit" value="Submit">
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="<?= base_url().'auth/login'?>" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Andcorpautos!</h1>
                  <p>©<?= date('Y');?> All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
</div>