<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span><?php echo lang('create_group_heading');?></h2>
			<div class="box-icon">
				<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<form class="form-horizontal" action="<?= base_url().'auth/create_group'?>" method="post">
				<fieldset>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Group Name</label>
					<div class="controls">
					  <input class="input-xlarge focused" name="group_name" id="focusedInput" type="text" placeholder="Group Name">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label" for="focusedInput">Description</label>
					<div class="controls">
                                <!-- <textarea name="description" id="" cols="10" rows="10"></textarea> -->
					  <input class="input-xlarge focused" name="description" id="focusedInput" type="text" placeholder="Description">
					</div>
				  </div>
				  <div class="form-actions">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<!-- <button class="btn">Cancel</button> -->
				  </div>
				</fieldset>
			  </form>
		
		</div>
	</div><!--/span-->	
</div><!--/row-->

    
<?php temp_view('groups',$groups);?>
