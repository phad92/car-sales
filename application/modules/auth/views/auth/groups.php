
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>List of Manufacturas/Make</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Group Name</th>
								  <th>Description</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
                          <?php //pp($groups);?>
                <?php foreach($groups as $group):?>
							<tr>
								<td><?= $group->name;?></td>
								<td><?= $group->description;?></td>
								<td class="center span6">
									<a class="btn btn-success" href="<?= base_url().'auth/edit_group/'.$group->id?>">
										Edit  
									</a>
									<a class="btn btn-danger" href="<?= base_url().'auth/delete_group/'.$group->id?>">
										Delete 
									</a>
								</td>
              </tr>
              <?php endforeach;?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

