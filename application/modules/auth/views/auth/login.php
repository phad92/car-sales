<div class="login_wrapper">
        <div class="form login_form">
          <?= ($message) ?? $message?>
          <section class="login_content">
            <form action="<?= base_url().'auth/login'?>" method="post">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" name="identity" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="password" required="" />
              </div>
              
              <div>
                <div class="checkbox">
                  <label><input type="checkbox" name="remember" value="1">Remember Me?</label>
                </div>
              </div>

              <div>
                <input type="submit" class="btn btn-default submit" name='submit' value="Login">
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="<?= base_url().'auth/register'?>" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Andcorpautos</h1>
                  <p>©<?= date('Y');?> All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        
      </div>

