<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_product extends CI_Model {

    private $_table;


    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('cs_product');
    }

    public function _product_detail(){
        $query_string = "cs_product.id,cs_product.model_year, cs_product.shipping_cost , cs_make.name AS `make`, cs_model.name AS `model`";
        $this->db->select($query_string);
        $this->db->from('cs_product');
        $this->db->join('cs_make','cs_product.make = cs_make.id');
        $this->db->join('cs_model','cs_product.model = cs_model.id');
        // $this->db->join('cs_color','cs_product.interior_color = cs_color.id');
        // $this->db->join('cs_color','cs_product.exterior_color = cs_color.id');
        return $this->db->get()->result();
    }
}
