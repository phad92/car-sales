

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
        <?= ($message) ?? $message?>
    <div class="x_panel">
      <div class="x_title">
        <h2><?= $headline?></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" action="<?= base_url().'product/update/'.$id?>" method="post" data-parsley-validate="" class="form-horizontal form-label-left" >
    
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Make <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="make" id="make" class="form-control select2_single">
                <option value="">Select Make</option>
                <?php foreach($makes as $row):?>
                  <option value="<?= $row->id?>" <?= selected_option($row->id, $make)?>><?= $row->name?></option>
                <?php endforeach?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model">Select Model <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="model" id="model" class="form-control select2_single">
                <option value="">Select Model</option>
                <?php foreach($models as $row):?>
                  <option value="<?= $row->id?>" <?= selected_option($row->id, $model)?>><?= $row->name?></option>
                <?php endforeach?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model_year">Model Year <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="model_year" name="model_year" value="<?= $model_year?>" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shipping_cost">Shipping Cost 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="shipping_cost" name="shipping_cost" required="required" value="<?= $shipping_cost?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inland_transport_cost"> Inland Transportation Cost  
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="inland_transport_cost" name="inland_transport_cost" value="<?= $inland_transport_cost?>" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inland_insurance">Inland insurance 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="inland_insurance" name="inland_insurance" value="<?= $inland_insurance?>" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="releases">Releases  
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="releases" name="releases" value="<?= $releases?>" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="estimated_duty">Estimated Duty  
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="estimated_duty" name="estimated_duty" value="<?= $estimated_duty?>" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vin">VIN 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="vin" name="vin" value="<?= $vin?>" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Miscellaneous 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" name="misc" value="<?= $misc?>" rows="3"></textarea>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>


        </form>
      </div>
    </div>
  </div>
</div>

<?php temp_view('products/list',$products);?>
