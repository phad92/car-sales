

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
        <?= ($message) ?? $message?>
    <div class="x_panel">
      <div class="x_title">
        <h2><?= $headline?></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" action="<?= base_url().'product/save'?>" method="post" data-parsley-validate="" class="form-horizontal form-label-left" >

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Make <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="make" id="make" class="form-control select2_single">
                <option value="">Select Make</option>
                <?php foreach($makes as $make):?>
                  <option value="<?= $make->id?>"><?= $make->name?></option>
                <?php endforeach?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model">Select Model <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="model" id="model" class="form-control select2_single">
                <option value="">Select Model</option>
                <?php foreach($models as $model):?>
                  <option value="<?= $model->id?>"><?= $model->name?></option>
                <?php endforeach?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model_year">Model Year <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="model_year" name="model_year" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="condition">Select Condition <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select id="condition" name="car_condition" class="form-control">
                    <option value="new">New</option>
                    <option value="used">Used</option>
                </select>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Select Category <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select id="category" name="category" class="form-control select2_single">
                    <option value="">Select Category</option>
                    <?php foreach($categories as $cat):?>
                        <option value="<?= $cat->id?>" ><?= $cat->name?></option>
                    <?php endforeach?>
                </select>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Select Transmission <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select id="transmission" name="transmission" class="form-control">
                    <option value="">Select Transmission</option>
                    <option value="automatic" >Automatic</option>
                    <option value="manual" >Manual</option>
                </select>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="color">Select Color
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select id="color" name="color" class="form-control select2_single">
                    <option value="">Select Color</option>
                    <?php foreach($colors as $color):?>
                        <option value="<?= $color->id?>"><?= $color->name?></option>
                    <?php endforeach?>
                </select>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="color">Select Fuel Type <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<select id="fuel_type" name="fuel_type" class="form-control">
                    <option value="">Select Fuel Type</option>
                    <?php foreach($fuel_types as $fuel_type):?>
                        <option value="<?= $fuel_type->id?>"><?= $fuel_type->name?></option>
                    <?php endforeach?>
                </select>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="doors">Doors 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="number" min="0" id="doors" name="doors" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="seats">Seats 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="number" min="0" id="seats" name="seats" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="seats">Mileage (Km) 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="number" min="0" id="mileage" name="mileage" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="seats">Horsepower 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="horse_power" name="horse_power" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" name="description" rows="3"></textarea>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php temp_view('products/list',$products);?>