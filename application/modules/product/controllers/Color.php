<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Color extends MX_Controller
{

  protected $model;
  private $url_segments = 'product/color/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('cs_color');
  }

  
    public function color($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $color = $this->model->get('name')->row();
        $data['headline'] = "Edit $color->name";
        $data['colors'] = $color;
        load_view($data);

    }

     public function add(){
         if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['colors'] = $this->model->get('name')->result();
          $data['headline'] = "Create Color";
          $data['message'] = get_flashdata();
          $data['view_file'] = 'color/create';
          load_admin($data);
      }

    public function save()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_color.name]');
        
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            // pp($data);
            if (!$this->model->_insert($data)) {

                $message = alert_message("Database operation failed", false);
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                
                $message = alert_message("Success", true);
                 set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = validation_errors('<div class="alert alert-danger">', '</div>');
             set_flashdata($message);
                redirect($this->url_segments.'add');
        }

    }

    public function edit($update_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $color = $this->model->get_where($update_id)->row();
        $data['colors'] = $this->model->get('name')->result();

        $data['name'] = $color->name;
        $data['id'] = $update_id;

        $data['headline'] = "Edit Color";
        $data['message'] = get_flashdata();
        $data['view_file'] = 'color/edit';
        load_admin($data);
    }

    public function update($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $data['name'] = $this->input->post('name');
        if ($this->form_validation->run()) {
            
            if (!$this->model->_update($update_id,$data)) {

                $message = alert_message("Database operation failed",false);
                
                set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
            }else{
                
                $message = alert_message("Success", true);
                 set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
            }
        } else {
            $message = validation_errors("<div class='alert alert-danger'>", "</div>");
            set_flashdata($message);
            redirect($this->url_segments.'edit/'.$update_id);
        }
    }

    public function delete($id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        if($this->model->_delete($id)){
              $message = 'delete successful';
              set_flashdata($message);
                  redirect($this->url_segments.'add');
          }
    }

}
