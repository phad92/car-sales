<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Make extends MX_Controller
{

    protected $model;
    private $url_segments = 'product/make/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('cs_make');
  }

  public function makes()
  {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['headline'] = "makes";
        $data['makes'] = $this->model->get('name')->result();
        load_admin($data);
    }

    public function make($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $make = $this->model->get('name')->row();
        $data['headline'] = "Edit $make->name";
        $data['makes'] = $make;
        load_view($data);

    }

     public function add(){
         if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['headline'] = "Add make";
          $data['message'] = get_flashdata();

          $data['makes'] = $this->model->get('name')->result();
          $data['view_file'] = 'make/create';
          load_admin($data);
      }

    public function save()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_make.name]');
        
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            print_r($data);
            if (!$this->model->_insert($data)) {

                $message = alert_message("Database operation failed", false);
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                
                $message = alert_message("Success", true);
                 set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = validation_errors("<div class='alert alert-danger'>",'</div>');
             set_flashdata($message);
                redirect($this->url_segments.'add');
        }

    }

    public function edit($update_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $make = $this->model->get_where($update_id)->row();
        $data['name'] = $make->name;
        $data['id'] = $update_id;
        $data['makes'] = $this->model->get('name')->result();

        $data['headline'] = "Add make";
        $data['message'] = get_flashdata();

        $data['view_file'] = 'make/edit';
        load_admin($data);
    }

    public function update($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_make.name]');
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            if (!$this->model->_update($update_id,$data)) {

                $message = alert_message("Database operation failed", false);
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
            }else{
                
                $message = alert_message("Success", true);
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = validation_errors('<div class="alert alert-danger">', '</div>');
             set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
        }
    }

    public function delete($id)
    {
       if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        if($this->model->_delete($id)){
              $message = alert_message('delete successful', true);
              set_flashdata($message);
                  redirect($this->url_segments.'add');
          }
    }

}
