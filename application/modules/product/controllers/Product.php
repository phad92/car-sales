<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Product extends MX_Controller
{

    protected $model;
    private $url_segments = 'product';

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('cs_product');
    }

    


    public function get_models(){
        $model = new main_model;
        $model->setTable('cs_model');
        return $model->get('name')->result();
    }

    public function get_makes(){
        $make = new main_model;
        $make->setTable('cs_make');
        return $make->get('name')->result();
    }

    public function get_categories(){
        $brand = new main_model;
        $brand->setTable('cs_category');
        return $brand->get('name')->result();
    }

    public function get_colors(){
        $color = new main_model;
        $color->setTable('cs_color');
        return $color->get('name')->result();
    }

    public function get_fuel_types(){
        $color = new main_model;
        $color->setTable('cs_fuel_type');
        return $color->get('name')->result();
    }

    public function products()
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['headline'] = "Products";
          $data['products'] = $this->model->get('make')->result();
          $data['view_file'] = 'products/list';
          load_admin($data);
      }

      public function product($update_id)
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $product = $this->model->get($update_id)->row();
          $data['headline'] = "Product $product->make";
          $data['product'] = $product;
          load_view($data);

      }

      public function add(){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['headline'] = "Create Product";
          $data['message'] = get_flashdata(); 
          $data['models'] = $this->get_models();
          $data['makes'] = $this->get_makes();
          $data['colors'] = $this->get_colors();
        //   $data['color2s'] = $this->get_colors();
          $data['categories'] = $this->get_categories();
          $data['fuel_types'] = $this->get_fuel_types();
        //   pp($data);
          $this->load->model('mdl_product');
          $data['products'] = $this->mdl_product->_product_detail();
          $data['load_select2'] = true;
          $data['view_file'] = 'products/create';
        //   pp($data);
          load_admin($data);
      }

      public function save()
      {

        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          
          $this->load->library('form_validation');
        //   $_POST = form_data();
          // $this->form_validation->set_rules()
              
            // if ($this->form_validation->run()) {
                $data = $this->_get_form_data();
                // pp($data);
              if (!$this->model->_insert($data)) {

                  $message = alert_message("Database operation failed", false);
                  set_flashdata($message);
                  redirect($this->url_segments.'/add');
              }else{
                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect($this->url_segments.'/add');
              }
          // } else {
          //     $message = validation_errors('<div class="alert alert-danger">', '</div>');
          //     pp($message);
          //     set_flashdata($message);
          //       redirect($this->url_segments.'/add');
          // }

      }

      public function edit($update_id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $product = $this->model->get_where($update_id)->row();
          
          $data = $this->_get_db_data($product);
        //pp($data);
          $data['models']     = $this->get_models();
          $data['makes']      = $this->get_makes();
          $data['colors']     = $this->get_colors();
          $data['categories'] = $this->get_categories();
          $data['fuel_types'] = $this->get_fuel_types();
          $data['id']         = $update_id;


        $this->load->model('mdl_product');

        $data['products'] = $this->mdl_product->_product_detail(); 
        $data['message'] = get_flashdata();
        $data['headline'] = "Edit $product->make";
        $data['view_file'] = 'products/edit';
        load_admin($data);
      }

      public function update($update_id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');
        
          
          if ($this->form_validation->run()) {
                $data = $this->_get_form_data($update_id);
                // pp($data);
              if (!$this->model->_update($update_id,$data)) {

                  $message = alert_message("Database operation failed", false);
                  set_flashdata($message);
                  redirect($this->url_segments.'/add');
              }else{

                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect($this->url_segments.'/add');
                }
            } else {
               $message = validation_errors('<div class="alert alert-danger">', '</div>');
               set_flashdata($message);
               $data['message'] = get_flashdata();
               $data['view_file'] = 'product edit';
                load_view($data);
          }
      }

      public function delete($id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          
          if($this->model->_delete($id)){
              $message = alert_message('delete successful', true);
              set_flashdata($message);
                  redirect($this->url_segments.'/products');
          }
      }

     function _get_db_data($product){
        $data['make'] = $product->make;
        $data['model'] = $product->model;
        $data['model_year'] = $product->model_year;
        $data['shipping_cost'] = $product->shipping_cost;
        $data['inland_transport_cost'] = $product->inland_transport_cost;
        $data['inland_insurance'] = $product->inland_insurance;
        $data['releases'] = $product->releases;
        $data['estimated_duty'] = $product->estimated_duty;
        $data['vin'] = $product->vin;
        $data['misc'] = $product->misc;
        // pp($data);
        return $data;
      }

      function _get_form_data($update_id = null){
        
        $data['code'] = uniq_id();
        $data['make'] = input_post('make');
        $data['model'] = input_post('model');
        $data['model_year'] = input_post('model_year');
        $data['shipping_cost'] = input_post('shipping_cost');
        $data['inland_transport_cost'] = input_post('inland_transport_cost');
        $data['inland_insurance'] = input_post('inland_insurance');
        $data['releases'] = input_post('releases');
        $data['estimated_duty'] = input_post('estimated_duty');
        $data['vin'] = input_post('vin');
        $data['misc'] = input_post('misc');
        $data['date_added'] = time();
        if(isset($update_id)){
            unset($data['code']);
            unset($data['date_added']);
        }

        return $data;
      }

  
}
