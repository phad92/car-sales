<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends MX_Controller
{

    protected $model;
    private $url_segments = 'product/product_model/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('model');
  }

  public function models()
  {
        
        $data['headline'] = "models";
        $data['models'] = $this->model->get('name')->result();
        
        load_view($data);
    }

    public function model($update_id)
    {
        $model = $this->model->get('name')->row();
        $data['headline'] = "Edit $model->name";
        $data['models'] = $model;
        load_view($data);

    }

     public function create(){
          $data['headline'] = "Create model";
          $data['message'] = get_flashdata();

          $data['models'] = $this->model->get('name')->result();
          $data['view_file'] = 'model/create';
          load_view($data);
      }

    public function save()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[model.name]');
        
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            if (!$this->model->_insert($data)) {

                $message = "Database operation failed";
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'create');
            }else{
                
                $message = "Success";
                 set_flashdata($message);
                redirect($this->url_segments.'create');
            }
        } else {
            $message = response_message(validation_errors());
             set_flashdata($message);
                redirect($this->url_segments.'create');
        }

    }

    public function edit($update_id){
        $model = $this->model->get_where($update_id)->row();
        $data['name'] = $model->name;
        $data['id'] = $update_id;

        $data['models'] = $this->model->get('name')->result();
        $data['headline'] = "Edit $model->name";
        $data['view_file'] = 'model/edit';
        load_view($data);
    }

    public function update($update_id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $data['name'] = $this->input->post('name');
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            if (!$this->model->_update($update_id,$data)) {

                $message = "Database operation failed";
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'create');
            }else{
                
                $message = "Success";
                 set_flashdata($message);
                redirect($this->url_segments.'create');
            }
        } else {
            $message = response_message(validation_errors());
             set_flashdata($message);
                redirect($this->url_segments.'create');
        }
    }

    public function delete($id)
    {

        if($this->model->_delete($id)){
              $message = 'delete successful';
              set_flashdata($message);
                  redirect($this->url_segments.'create');
          }
    }

}
