<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Fuel_type extends MX_Controller
{

    protected $model;
    private $url_segments = 'product/fuel_type/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('cs_fuel_type');
  }


    public function fuel_type($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $fuel_type = $this->model->get('name')->row();
        $data['headline'] = "Edit $fuel_type->name";
        $data['fuel_types'] = $fuel_type;
        $data['view_file'] = 'detail';
        load_view($data);

    }

     public function add(){
         if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['headline'] = "Create Fuel Type";
          $data['message'] = get_flashdata();

          $data['fuel_types'] = $this->model->get('name')->result();
          $data['view_file'] = 'fuel_type/create';
          load_admin($data);
      }

    public function save()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_fuel_type.name]');
        
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            
            if (!$this->model->_insert($data)) {

                $message = "Database operation failed";
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                
                $message = "Success";
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = response_message(validation_errors());
            set_flashdata($message);
            redirect($this->url_segments.'add');
        }

    }

    public function edit($update_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $fuel_type = $this->model->get_where($update_id)->row();
        $data['name'] = $fuel_type->name;
        $data['id'] = $update_id;

        $data['fuel_types'] = $this->model->get('name')->result();
        $data['headline'] = "Edit Fuel Type";
        $data['message'] = get_flashdata();
        $data['view_file'] = 'fuel_type/edit';
        load_admin($data);
    }

    public function update($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            if (!$this->model->_update($update_id,$data)) {

                $message = "Database operation failed";
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'add');

            }else{
                
                $message = "Success";
                set_flashdata($message);
                redirect($this->url_segments.'add');
                
            }
        } else {
            $message = response_message(validation_errors());
            set_flashdata($message);
            redirect($this->url_segments.'add');
        }
    }

    public function delete($id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        if($this->model->_delete($id)){
            $message = 'delete successful';
            set_flashdata($message);
            redirect($this->url_segments.'add');
        }
    }

}
