<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Category extends MX_Controller
{

    protected $model;
    private $url_segments = 'product/category/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('cs_category');
  }

    public function category($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $category = $this->model->get('name')->row();
        $data['headline'] = "Edit $category->name";
        $data['categorys'] = $category;
        load_admin($data);

    }

     public function add(){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['headline'] = "Create category";
          $data['message'] = get_flashdata();

          $data['categories'] = $this->model->get('name')->result();
          $data['view_file'] = 'category/create';
          $data['load_datatable'] = true;
          load_admin($data);
      }

    public function save()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_category.name]');
        
        if ($this->form_validation->run()) {
            
            $data['name'] = $this->input->post('name');

            if (!$this->model->_insert($data)) {
                $message = alert_message("Database operation failed", false);
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                $message = alert_message("Success", true);
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = validation_errors("<div class='alert alert-danger'>","</div>");
             set_flashdata($message);
            redirect($this->url_segments.'add');
        }

    }

    public function edit($update_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $category = $this->model->get_where($update_id)->row();
        $data['name'] = $category->name;
        $data['id'] = $update_id;

        $data['message'] = get_flashdata();
        $data['headline'] = 'Edit Category';
        $data['categories'] = $this->model->get('name')->result();
        $data['view_file'] = 'category/edit';
        load_admin($data);
    }

    public function update($update_id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            if (!$this->model->_update($update_id,$data)) {

                $message = "Database operation failed";
                $alert = alert_message($message, false);
                set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
            }else{
                
                $message = alert_message('success', true);
                 set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
            }
        } else {
            $message = validation_errors('<div class="alert alert-danger">','</div>');
             set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
        }
    }

    public function delete($id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
       
        if($this->model->_delete($id)){
              $message = 'delete successful';
              set_flashdata($message);
              redirect($this->url_segments.'add');
          }
    }

}
