<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white edit"></i><span class="break"></span><?= $headline?></h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="<?= base_url().'track_order/update_state/'.$id?>" method="post">
							<fieldset>

                                <div class="control-group">
                                    <label class="control-label" for="status">Selete State</label>
                                    <div class="controls">
                                        <select name="current_status" id="status" class="form-control">
                                            <option value="">Select State</option>
                                            <?php foreach($tracking_states as $key => $value):?>
                                                <option value="<?= $key?>" <?= selected_option($key, $current_status)?>><?= $value?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                            
                                <div class="control-group">
                                    <label class="control-label" for="comment">Comment</label>
                                    <div class="controls">
                                        <textarea class="" id="comment" rows="3"><?= $comment?></textarea>
                                    </div>
                                </div>

                            

							<div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save changes</button>
							</div>
							</fieldset>
						  </form>
					</div>
				</div><!--/span-->
			
            </div><!--/row-->
 <?php temp_view('list',$track_orders);?>           