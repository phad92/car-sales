
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>List of Manufacturas/Make</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Code</th>
								  <th>Client</th>
								  <th>Created By</th>
								  <th>Current Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
				<?php foreach($track_orders as $state):
						?>
							<tr>
								<td><?= $state->code;?></td>
								<td><?= account_name('clients',$state->client);?></td>
								<td><?= account_name('staffs',$state->created_by);?></td>
								<td><?= readable_current_state($state->current_status);?></td>
								<td class="center span6">
									<a class="btn btn-success" href="<?= base_url().'track_order/edit_state/'.$state->id?>">
										Update State  
									</a>
									<a class="btn btn-danger" href="<?= base_url().'track_order/edit_state/'.$state->id?>">
										Delete State  
									</a>
								</td>
              </tr>
              <?php endforeach;?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

