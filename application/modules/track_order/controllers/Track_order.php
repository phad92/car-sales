<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Track_order extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('cs_track_order');
    }

    function current_state($state){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $status = str_replace('_',' ',$state);
        $status_index = $this->_status_index($status);
        $data['track_orders'] = $this->model->get_where_custom('current_status',$status_index)->result();
        $data['headline'] = "current status <strong>$status</strong>";
        $data['view_file'] = 'list';
        load_admin($data);
    }

    function _status_index($status){
        $this->load->config('app_config');

        $order_tracking = $this->config->item('order_tracking');
        foreach($order_tracking as $key => $value){
            if($status == $value){
                return $key;
            }
        }
    }

    function check_state($order_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $order = foreign_row('cs_order',$order_id);
        $track_order = $this->model->get_where_custom('cs_order',$order->id);
        $data['order'] = $order;
        $data['track_order'] = $track_order;

        $data['headline'] = "Track Order $order->code";
        $data['view_file'] = 'detail';
        load_admin($data);
    }

    function start_tracking($order_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $order = foreign_row('cs_order',$order_id);
        $data['code'] = uniq_id(8);
        $data['cs_order'] = $order->id;
        $data['client'] = $order->client;
        $data['created_by'] = $order->staff;
        $data['current_status'] = 0;
        $data['date_created'] = time();
        $model = new main_model;
        $model->setTable('cs_track_order');
        return $model->_insert($data);
    }

    public function order_exists($order_id){
        $order = $this->model->get_where_custom('cs_order',$order_id);

        if($order->num_rows() < 1){
            return false;
        }

        return true;
    }

    public function edit_state($update_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->config('app_config');
        $track_order = $this->model->get_where($update_id)->row();
        $data = $this->_get_db_data($track_order);
        $tracking_code = strtoupper($track_order->code);
        $data['tracking_states'] = $this->config->item('order_tracking');
        $data['track_orders'] = $this->model->get('id')->result();       
        $data['headline'] = "Update Current Tracking Status of $tracking_code";
        $data['id'] = $update_id;
        $data['view_file'] = 'edit';
        load_admin($data);
    }


    public function update_state($update_id){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->config('app_config');
        $data['current_status'] = $this->input->post('current_status');
        $data['comment'] = $this->input->post('comment');
        $data['date_updated'] = time();
        // pp($data);
        if($this->model->_update($update_id,$data)){
            redirect('track_order/edit_state/'.$update_id);
        }else{
            set_flashdata('Failed to update tracking status');
            redirect('track_order/edit_state/'.$update_id);
        }
        
    }


    public function orders()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['headline'] = "Track_orders";
        $data['view_file'] = "list";
        $data['orders'] = $this->model->get('date_added')->result();
        load_admin($data);
    }

    
    
    public function track_order($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['track_order'] = $this->model->get_where($update_id)->row_array();
        $data['id'] = $update_id;
        $this->load->model('mdl_order','pm');
        
        $data['orders'] = $this->pm->_order_details();
        $data['headline'] = 'Track_order Detail';
        $data['view_file'] = 'detail';
        load_view($data);
    }

    public function create(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $this->load->model('mdl_order');
        $this->load->model('auth/mdl_auth');
        $data['orders'] = $this->model->get('code')->result();
        $data['products'] = $this->fetch_foreign_data('cs_product','name');
        $data['staffs'] = $this->mdl_auth->get_accounts_by_group('staffs');
        $data['clients'] = $this->mdl_auth->get_accounts_by_group('clients');
        $data['message'] = get_flashdata();
        $data['headline'] = 'Create Track_order';
        $data['view_file'] = 'create';
        load_admin($data);
    }

    public function save()
      {
          $this->load->library('form_validation');

          $this->form_validation->set_rules('product', 'Product', 'trim|required');
        
        if ($this->form_validation->run()) {
            $data = $this->_get_form_data();
            // pp($data);
            if (!$this->model->_insert($data)) {
                  $message = "Database operation failed";
                  set_flashdata($message);
                  redirect('track_order/create');
              }else{
                //   pp(input_post('proceedToPayment'));
                  if(input_post('proceedToPayment')){
                      $last_order = $this->model->get_max();
                      $redirect = 'payment/make_payment?track_order='.$last_order;
                      redirect($redirect);
                  }
                  $message = "Success";
                  set_flashdata($message);
                  redirect('track_order/create');
            }
          } else {
              $message = validation_errors();
              set_flashdata($message);
                redirect('track_order/create');
          }

      }

      public function edit($update_id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->model('mdl_order');
          $track_order = $this->model->get_where($update_id)->row();
          $data = $this->_get_db_data($track_order);
          
          $data['orders'] = $this->model->get('code');
          $data['staffs'] = $this->get_staffs();
          
          $data['headline'] = "Edit $track_order->name";
          $data['view_file'] = 'edit';
          load_view($data);
      }

      public function update($update_id)
      {
          $this->load->library('form_validation');

          $this->form_validation->set_rules('product', 'Product', 'trim|required');
          
          if ($this->form_validation->run()) {
                $data = $this->_get_form_data();

              if (!$this->model->_update($update_id,$data)) {

                  $message = response_message("Database operation failed");
                  print_r($message);die();
                  _api_return_token($message, false);
              }else{

                  $message = response_message("Success");;
                  print_r($message);die();
                  _api_return_token($message);
                }
            } else {
                $message = response_message(validation_errors());
                print_r($message);die();
              _api_return_token($message, false);
          }
      }

      public function delete($id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          if($this->model->_delete($id)){
              $message = response_message('delete successful');
              set_flashdata($message);
                  redirect($this->url_segments.'/orders');
          }
      }


    function _get_db_data($track_order){
        $data['id'] = $track_order->id;
        $data['code'] = $track_order->code;
        $data['cs_order'] = $track_order->cs_order;
        $data['client'] = $track_order->client;
        $data['created_by'] = $track_order->created_by;
        $data['current_status'] = $track_order->current_status;
        $data['comment'] = $track_order->comment;
        return $data;
      }

      function _get_form_data($update_id = null){
        // added the staff using session
        $data['code'] = uniq_id(8);
        $data['current_status'] = input_post('current_status');
        $data['comment'] = input_post('comment');
        $data['date_added'] = time();
        
        if(isset($update_id)){
            unset($data['code']);
            unset($data['date_added']);
            $data['date_updated'] = time();
        }

        return $data;
      }


  
}
