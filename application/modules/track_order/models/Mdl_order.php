<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_order extends CI_Model {

function __construct() {
parent::__construct();
}

public function _service_details(){
    $query_string = "service.id, service.name, service.price, CONCAT(employee.firstname, ' ' ,employee.lastname) AS employee";

    $this->db->select($query_string);
    $this->db->from('service');
    $this->db->join('employee','service.employee = employee.code');
    return $this->db->get()->result();
}
}
