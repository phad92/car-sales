


<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
  <div class="x_title">
    <h2>Payment History</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Settings 1</a>
          </li>
          <li><a href="#">Settings 2</a>
          </li>
        </ul>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <!-- <p class="text-muted font-13 m-b-30">
      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
    </p> -->
				<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                    <thead>
                      <tr role="row">
						<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 10px;">#</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Code</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Client</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Amount</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Status</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Payment Method</th>
						<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 157px;">Date Added</th>
						<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 90px;">Action</th>
					</tr>
                    </thead>
                    <tbody>
					  <?php $count=0; foreach($payments as $payment): 
						$order = foreign_row('cs_order',$payment->cs_order);
						// pp($order);
						$account = fetch_account('clients',$order->client);
						$count++;?>
						<tr role="row" class="odd">
							<td><?= $count;?></td>
							<td><?= $payment->code;?></td>
							<td><?= $account->first_name .' '.$account->last_name;?></td>
							<td><?= "GHC ". $payment->amount;?></td>
							<td><?= $payment->payment_status;?></td>
							<td><?= $payment->payment_method;?></td>
							<td><?= date('d-m-Y',$payment->date_added);?></td>
							<td>
								<a href="<?= base_url().'order/order/'.$order->id?>" class="btn btn-info btn-xs">View Order </a>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
                  </table></div></div>
                </div>
              </div>
            </div>

<!-- OLD -->
