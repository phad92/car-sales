<div class="container">
    <div class="row" style="margin: 24px 0">
        <div class="col-md-8 offset-md-2">
            <form action="<?= base_url().'service/update/'.$id?>" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="<?= $name?>" placeholder="Enter Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Price (if quantified price per quantity)</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="price" value="<?= $price?>" placeholder="Enter Price">
                </div>
                <!-- add quatify checkbox to check type of service that can be measured by quantity -->
                <!-- <div class="form-group">
                    <label for="">Quantified ?</label>
                    <input type="checkbox" name="quantify"value="1" id="">
                </div> -->
                <!-- <div> -->
                    <div class="form-group">
                        <label for="">Staff</label>
                        <select name="employee" id="" class="form-control">
                            <option value="">Select Staff</option>
                            <?php foreach($employees as $emp):?>
                                <option value="<?= $emp->code?>" <?php if($emp->code == $employee){ echo 'selected';}?>><?= $emp->firstname .' '.$emp->lastname?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                <!-- </div> -->
                <div class="form-group">
                    <label for="exampleInputEmail1">Timeframe</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="timeframe" value="<?= $timeframe?>" placeholder="Enter timeframe">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Comment</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="comment" value="<?= $comment?>" placeholder="Enter comment">
                </div>
                <button type="submit" class="btn btn-primary float-right">Submit</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <?php temp_view('list',$services);?>
        </div>
    </div>
</div>