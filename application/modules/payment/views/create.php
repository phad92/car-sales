


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
        <?= ($message) ?? $message?>
    <div class="x_panel">
        <a href="<?= base_url().'payment/payments'?>" class="btn btn-info">Payment History</a>
      <div class="x_title">
        <h2><?= $headline?></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" action="<?= base_url().'payment/save'?>" method="post" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

            <?php if(!$order_id):?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Staff <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="cs_order" id="cs_order" class="form-control">
                            <option value="">Select Order</option>
                            <?php foreach($orders as $order):
                                $client = fetch_account('clients',$order->client)?>
                                <option value="<?= $order->id?>"><?= $order->code .' - '. ucwords($client->first_name.' '. $client->last_name) ?></option>
                            <?php endforeach?>
                    </select>
                    </div>
                </div>
            <?php else: ?>
                <input type="hidden" name="cs_order" value="<?= $order_id?>">
            <?php endif?>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="amount" name="amount" required="required" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Payment Method <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="payment_method" id="payment_method" class="form-control">
                        <option value="">Select Payment Method</option>
                        <option value="cash">Cash</option>
                        <option value="cheque">Cheque</option>
                    </select>
                </select>
                </div>
            </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cheque_num">Cheque No.
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="cheque_num" name="cheque_num" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" name="comment" rows="3"></textarea>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary">Save Payment</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
    
