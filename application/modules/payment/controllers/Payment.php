<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Payment extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('cs_payment');
    }

    public function fetch_foreign_data($table,$order_by){
        $model = new main_model;
        $model->setTable($table);
        return $model->get($order_by)->result();
    }

    // private access
    public function history(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

        $this->load->model('Mdl_payment','mp');
        $client = $_GET['client'];
        $client_payments = $this->mp->history($client);

        $data['payments'] = $client_payments;
        $data['headline'] = "Clients Payment History";
        $data['view_file'] = 'history';
        load_admin($data);

    }

    public function payments()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['hedadline'] = "Payments";
        $data['payments'] = $this->model->get('date_added')->result();
        $data['view_file'] = "list";
        load_admin($data);
    }

    public function payment($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

        $data['payment'] = $this->model->get_where($update_id)->row_array();
        $data['id'] = $update_id;
        $data['headline'] = 'Payment Detail';
        $data['view_file'] = 'detail';
        load_view($data);
    }

    // public function add()

    public function make_payment(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

        $order = get_post('order');
        
        $this->load->model('mdl_payment');
        $this->load->model('auth/mdl_auth');
        $data['payments'] = $this->model->get('code')->result();
        $data['orders'] = $this->fetch_foreign_data('cs_order','code');
        // pp($data['orders']);
        $data['message'] = get_flashdata();
        if($order){
            $data['order_id'] = $order;
            $data['headline'] = 'Create Payment for Order '.$order;
        }else{
            $data['order_id'] = '';
            $data['headline'] = 'Create Payment';
        }
        $data['view_file'] = 'create';
        load_admin($data);
    }

      public function save()
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');

          $this->form_validation->set_rules('amount', 'Amount Payed', 'trim|required');
          
          $data = $this->_get_form_data();
        //   pp($data);
        if ($this->form_validation->run()) {
          if (!$this->model->_insert($data)) {
              $message = "Database operation failed";
                  $alert = alert_message($message,'danger');
                  set_flashdata($alert);
                  redirect('payment/make_payment?order='.$data['cs_order']);
              }else{
                  $message = "Success";
                  $alert = alert_message($message,'success');
                  set_flashdata($alert);
                   redirect('payment/make_payment?order='.$data['cs_order']);
                //   redirect('order/create');
              }
          } else {
              $message = validation_errors('<span class="alert alert-danger">','</span>');
              set_flashdata($message);
               redirect('payment/make_payment?order='.$data['cs_order']);
          }

      }

      public function edit($update_id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

          $this->load->model('mdl_payment');
          $payment = $this->model->get_where($update_id)->row();
          $data = $this->_get_db_data($payment);
          
          $data['payments'] = $this->model->get('code');
          $data['staffs'] = $this->get_staffs();
          
          $data['headline'] = "Edit $payment->name";
          $data['view_file'] = 'edit';
          load_view($data);
      }

      public function update($update_id)
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');

          $this->form_validation->set_rules('product', 'Product', 'trim|required');
          
          if ($this->form_validation->run()) {
                $data = $this->_get_form_data();

              if (!$this->model->_update($update_id,$data)) {

                  $message = response_message("Database operation failed");
                  print_r($message);die();
                  _api_return_token($message, false);
              }else{

                  $message = response_message("Success");;
                  print_r($message);die();
                  _api_return_token($message);
                }
            } else {
                $message = response_message(validation_errors());
                print_r($message);die();
              _api_return_token($message, false);
          }
      }

      public function delete($id){
            if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          if($this->model->_delete($id)){
              $message = response_message('delete successful');
              set_flashdata($message);
                  redirect($this->url_segments.'/payments');
          }
      }


    function _get_db_data($payment){
        $data['id'] = $payment->id;
        $data['code'] = $payment->code;
        $data['cs_order'] = $payment->cs_order;
        $data['amount'] = $payment->amount;
        $data['payment_status'] = $payment->payment_status;
        $data['payment_method'] = $payment->payment_method;
        $data['cheque_num'] = $payment->cheque_num;
        $data['comment'] = $payment->comment;
        return $data;
      }

      function _get_form_data($update_id = null){
        // added the staff using session
        $data['code'] = uniq_id(6);
        $data['amount'] = input_post('amount');
        $data['cs_order'] = input_post('cs_order');
        $data['payment_status'] = input_post('payment_status');
        $data['payment_method'] = input_post('payment_method');
        $data['cheque_num'] = input_post('cheque_num');
        $data['comment'] = input_post('comment');
        $data['date_added'] = time();

        if($data['amount']){
            $data['payment_status'] = 1;
        }else{
            $data['payment_status'] = 0;
        }

        if(!$data['payment_method'] || $data['payment_method'] !== 'cheque'){
            $data['cheque_num'] = '';
        }

        if(isset($update_id)){
            unset($data['code']);
            unset($data['uniq_id']);
            unset($data['date_added']);
        }

        return $data;
      }


  
}
