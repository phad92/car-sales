<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Template extends MX_Controller
{

function __construct() {
parent::__construct();
}

public function index(){
    // echo 'hello world';
    $data['view_file'] = 'index';
    $this->admin_layout($data);
}

function auth_layout($data){
    if(!isset($data['view_module'])){
        $data['view_module'] = $this->uri->segment(1);
    }
    
    $this->load->view('admin/auth/layout',$data);
}

function public_layout($data){
    if(!isset($data['view_module'])){
        $data['view_module'] = $this->uri->segment(1);
    }
    
    $this->load->view('layout',$data);
}

function admin_layout($data){
    // // $data = array();
    if(!isset($data['view_module'])){
        $data['view_module'] = $this->uri->segment(1);
    }
    
    $this->load->view('admin/admin_layout',$data);
}

function admin()
{
    $this->load->view('admin/admin_layout');

}

}