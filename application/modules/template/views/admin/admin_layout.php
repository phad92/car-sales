<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?= base_url()?>public/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url()?>public/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url()?>public/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <?php if(isset($load_select2)):?>
      <!-- Select 2 -->
      <link href="<?= base_url()?>public/assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <?php endif?>

    <?php if(isset($load_icheck)):?>
      <!-- iCheck -->
      <link href="<?= base_url()?>public/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <?php endif?>

    <?php if(isset($load_form_assets)):?>
      <!-- bootstrap-daterangepicker -->
      <link href="<?= base_url()?>public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <?php endif?>

    <?php if(isset($load_datatable)):?>
      <!-- Datatables -->
      <link href="<?= base_url()?>public/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="<?= base_url()?>public/assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
      <link href="<?= base_url()?>public/assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
      <link href="<?= base_url()?>public/assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
      <link href="<?= base_url()?>public/assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <?php endif?>


    <!-- Custom Theme Style -->
    <link href="<?= base_url()?>public/assets/build/css/custom.min.css" rel="stylesheet">
  </head>

<body>
		<!-- top header -->
		<?php $this->load->view('admin/header')?>
		<!-- top navigation -->
		<?php $this->load->view('admin/nav')?>
		<div class="right_col" role="main">
		<!-- page content -->
			<?php if(isset($view_file)){
				$this->load->view($view_module.'/'.$view_file);
			}?>
      </div>
			<!-- footer -->
		<?php $this->load->view('admin/footer')?>
</body>
</html>
