<!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?= base_url()?>dashboard"><i class="fa fa-home"></i> Dashboard </a></li>
                  <!-- <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>dashboard">Dashboard</a></li>
                      
                    </ul>
                  </li> -->
                  <li><a><i class="fa fa-edit"></i> Mangage Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>product/add">Add Product</a></li>
                      <li><a href="<?= base_url()?>product/products">Show Products</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Product Features <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>product/make/add">Add Make</a></li>
                      <li><a href="<?= base_url()?>product/model/add">Add Model</a></li>
                      <li><a href="<?= base_url()?>product/category/add">Add Category</a></li>
                      <li><a href="<?= base_url()?>product/color/add">Add Color</a></li>
                      <li><a href="<?= base_url()?>product/fuel_type/add">Add Fuel Type</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Manage Customers <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>customer/add">Add Customer</a></li>
                      <li><a href="<?= base_url()?>customer/customers">Show Customers</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Services <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>service/add">Add Service</a></li>
                      <li><a href="<?= base_url()?>service/services">Show Services</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-clone"></i>Manage Orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>order/create">Create Order</a></li>
                      <li><a href="<?= base_url()?>order/orders">Show Orders</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>Admin</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Manage Staffs <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>staff/add">Add Staff</a></li>
                      <li><a href="<?= base_url()?>staff/staffs">Show Staffs</a></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Payments <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?= base_url()?>payment/make_payment">Add Payment</a></li>
                      <li><a href="<?= base_url()?>payment/payments">Payment History</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Manage Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?= base_url()?>auth/create_group">User Groups</a>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <!-- <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li> -->
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

		