  <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url()?>public/assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url()?>public/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url()?>public/assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= base_url()?>public/assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= base_url()?>public/assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?= base_url()?>public/assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    
    <?php if(isset($load_select2)):?>
      <!-- Select2 -->
      <script src="<?= base_url()?>public/assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <?php endif?>

    <?php if(isset($load_icheck)):?>
      <!-- iCheck -->
      <script src="<?= base_url()?>public/assets/vendors/iCheck/icheck.min.js"></script>
    <?php endif?>

    <?php if(isset($load_flot)):?>
      <!-- Flot -->
      <script src="<?= base_url()?>public/assets/vendors/Flot/jquery.flot.js"></script>
      <script src="<?= base_url()?>public/assets/vendors/Flot/jquery.flot.pie.js"></script>
      <script src="<?= base_url()?>public/assets/vendors/Flot/jquery.flot.time.js"></script>
      <script src="<?= base_url()?>public/assets/vendors/Flot/jquery.flot.stack.js"></script>
      <script src="<?= base_url()?>public/assets/vendors/Flot/jquery.flot.resize.js"></script>
      <!-- Flot plugins -->
      <script src="<?= base_url()?>public/assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
      <script src="<?= base_url()?>public/assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
      <script src="<?= base_url()?>public/assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <?php endif?>

    <!-- DateJS -->
    <script src="<?= base_url()?>public/assets/vendors/DateJS/build/date.js"></script>
    <?php if(isset($load_form_assets)):?>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= base_url()?>public/assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <?php endif?>
    <?php if(isset($load_datatable)):?>
    <!-- Datatables -->
    <script src="<?= base_url()?>public/assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= base_url()?>public/assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <?php endif?>

    <!-- Custom Theme Scripts -->
    <script src="<?= base_url()?>public/assets/build/js/custom.js"></script>
 