<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Appointment extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('appointment');
    }

    public function get()
      {
          // $model = $this->model;
          $payload = $this->model->get('code')->result();
          $config_data = [
              // 'key' => ['header', $this->api_key],
              'methods' => ['GET'],
          ];
          _api_config($config_data);
          pp($payload);
          _api_return_token($payload);
          // $this->api_library->_api_return_token($payload);
      }

      public function get_by_id($update_id)
      {
          $payload = $this->model->get_where($update_id)->row_array();
          // print_r($payload);die();
          _api_config([
              // 'key' => ['header', $this->api_key],
              'methods' => ['GET'],
          ]);
            pp($payload);
          _api_return_token($payload);

      }

      public function save()
      {
          $this->load->library('form_validation');
          $_POST = form_data();
        //   $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[color.name]');
          _api_config([
              // 'key' => ['header', $this->api_key],
              'methods' => ['POST'],
              ]);
              
            // if ($this->form_validation->run()) {
                //add payment status field
                $data['code'] = uniq_id();
                $data['booking_date'] = input_post('booking_date');
                $data['booking_type'] = input_post('booking_type');
                $data['employee'] = input_post('employee');
                $data['service'] = input_post('service');
                $data['booking'] = input_post('booking');
                $data['start_time'] = input_post('start_time');
                $data['timeframe'] = input_post('timeframe');
                $data['date_added'] = time();

              if (!$this->model->_insert($data)) {
                  $message = response_message("Database operation failed");
                  print_r($message);die();
                  _api_return_token($message, false);
              }else{
                  $message = response_message("Success");;
                  print_r($message);die();
                  _api_return_token($message);
              }

        //   } else {
        //       $message = response_message(validation_errors());
        //       pp($message);
        //       _api_return_token($message, false);
        //   }

      }

      public function update($update_id)
      {
          $this->load->library('form_validation');
          $_POST = form_data();
          $this->form_validation->set_rules('name', 'Name', 'trim|required');
          _api_config([
              // 'key' => ['header', $this->api_key],
              'methods' => ['POST'],
          ]);
          if(!$update_id){
              $message = "Invalid id";
              $this->_api_return_token($message, false);
          }

        //   if ($this->form_validation->run()) {
                $data['code'] = uniq_id(5);
                $data['status'] = input_post('status');
                $data['customer'] = input_post('customer');
                $data['payment_method'] = input_post('payment_method');
                $data['date_added'] = time();
              if (!$this->model->_update($update_id,$data)) {

                  $message = response_message("Database operation failed");
                  print_r($message);die();
                  _api_return_token($message, false);
              }else{

                  $message = response_message("Success");;
                  print_r($message);die();
                  _api_return_token($message);
                }
        //     } else {
        //         $message = response_message(validation_errors());
        //         print_r($message);die();
        //       _api_return_token($message, false);
        //   }
      }

      public function delete($id){
          _api_config([
              // 'key' => ['header', $this->api_key],
              'methods' => ['POST'],
          ]);
          if($this->model->_delete($id)){
              $message = response_message('delete successful');
              pp($message);
              _api_return_token($message);
          }
      }

  
}
