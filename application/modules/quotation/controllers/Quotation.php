<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Quotation extends MX_Controller
{

    protected $model;
    private $url_segments = 'product/model/';

  public function __construct()
  {
    parent::__construct();
    $this->model = new main_model;
    $this->model->setTable('cs_quotation');
  }

  public function quotations()
  {
        
        $data['headline'] = "models";
        $data['quotations'] = $this->model->get('id')->result();
        load_view($data);
    }

    public function quotation($update_id)
    {
        $quote = $this->quote->get('id')->row();
        $data['headline'] = "Edit $quote->name";
        $data['quotation'] = $quotation;
        load_view($data);

    }

     public function add(){
          $data['headline'] = "Create model";
          $data['message'] = get_flashdata();
          $this->load->module('product');
          $data['makes'] = foreign_result('cs_make','name');
          $data['models'] = $this->model->get('name')->result();
          $data['models'] = $this->model->get('name')->result();
          $data['view_file'] = 'model/create';
          load_admin($data);
      }

    public function save()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_model.name]');
        
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            $data['make'] = $this->input->post('make');
            if (!$this->model->_insert($data)) {

                $message = alert_message("Database operation failed", false);
                // print_r($message);die();
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                
                $message = alert_message("Success", true);
                 set_flashdata($message);
                redirect($this->url_segments.'add');
            }
        } else {
            $message = validation_errors('<div class="alert alert-danger">', '</div>');
             set_flashdata($message);
                redirect($this->url_segments.'add');
        }

    }

    public function edit($update_id){
        $model = $this->model->get_where($update_id)->row();
        $data['name'] = $model->name;
        $data['make'] = $model->make;
        $data['id'] = $update_id;
        // pp($data);
        $data['models'] = $this->model->get('name')->result();
        $data['products'] = foreign_result('cs_product','name');
        $data['services'] = foreign_result('cs_service','name');
        // pp($data['makes']);
        $data['headline'] = "Make a qoute";
        $data['message'] = get_flashdata();
        $data['view_file'] = 'model/edit';
        load_admin($data);
    }

    public function update($update_id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        if ($this->form_validation->run()) {
            $data['name'] = $this->input->post('name');
            $data['make'] = $this->input->post('make');
            if (!$this->model->_update($update_id,$data)) {
                $message = alert_message("Database operation failed", false);
                set_flashdata($message);
                redirect($this->url_segments.'add');
            }else{
                $message = alert_message("Success", true);
                set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
            }
        } else {
            $message = validation_errors('<div class="alert alert-danger">','</div>');
             set_flashdata($message);
                redirect($this->url_segments.'edit/'.$update_id);
        }
    }

    public function delete($id)
    {
       
        if($this->model->_delete($id)){
              $message = alert_message('delete successful', true);
              set_flashdata($message);
              redirect($this->url_segments.'add');
          }
    }

}
