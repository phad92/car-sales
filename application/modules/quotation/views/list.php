
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white user"></i><span class="break"></span>List of Manufacturas/Make</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Code</th>
								  <th>Product</th>
								  <th>quantity</th>
								  <th>price</th>
								  <th>Client</th>
								  <th>Status</th>
								  <th>Approved</th>
								  <th>Current Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
				<?php foreach($orders as $order):
					$current_state_index = get_current_tracking_status($order->id);
					$product = foreign_row('cs_product',$order->product)?>
							<tr>
								<td><?= $order->code;?></td>
								<td><?= $product->name;?></td>
								<td><?= $order->quantity;?></td>
								<td><?= 'Ghc'. $product->price;?></td>
								<td><?= account_name('clients',$order->client);?></td>
								<td><?= $order->status;?></td>
								<td><?= ($order->approved) ? 'Yes' : 'No';?></td>
								<td><?= readable_current_state($current_state_index);?></td>
								<td class="center span6">
									<a class="btn btn-info" href="<?= base_url().'track_order/check_state/'.$order->id?>">
										Track
									</a>
									<a class="btn btn-success" href="<?= base_url().'order/edit/'.$order->id?>">
										Edit  
									</a>
									<a class="btn btn-danger" href="<?= base_url().'order/delete/'.$order->id?>">
										Delete 
									</a>
								</td>
              </tr>
              <?php endforeach;?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

