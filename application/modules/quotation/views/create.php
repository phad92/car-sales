

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
        <?= ($message) ?? $message?>
    <div class="x_panel">
      <div class="x_title">
        <h2><?= $headline?></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" action="<?= base_url().'service/save'?>" method="post" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product">Select Product <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
							<select name="staff" id="staff" class="form-control">
                <option value="">Select Product</option>
                <?php foreach($products as $product):?>
                  <option value="<?= $product->id?>"><?= $product->name?></option>
                <?php endforeach?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantity">Quantity <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="quantity" name="quantity" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Staff <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
							<select name="staff" id="staff" class="form-control">
                <option value="">Select Staff</option>
                <?php foreach($staffs as $staff):?>
                  <option value="<?= $staff->id?>"><?= $staff->first_name . ' ' . $staff->last_name?></option>
                <?php endforeach?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="timeframe">Time Frame <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="timeframe" name="timeframe" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" name="comment" rows="3"></textarea>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
    
<?php temp_view('list',$quotations);?>








    <!-- OLD -->
<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Create Quotation</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="<?= base_url().'quotation/save'?>" method="post">
							<fieldset>

                            <!-- <div class="row"> -->
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Client</label>
                                    <?php //pp($makes)?>
                                    <div class="controls">
                                        <select id="client" name="client" data-rel="chosen">
                                            <option value="">Select Client</option>
                                            <?php foreach($clients as $client):?>
                                                <option value="<?= $client->id?>"><?= $client->first_name .' ' .$client->last_name?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="control-group">
									<label class="control-label" for="focusedInput">Quantity</label>
									<div class="controls">
										<input class="input-xlarge focused" name="quantity" id="focusedInput" type="number" min="1" value="1" placeholder="Phone">
									</div>
							  </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Staff</label>
                                    <?php //pp($makes)?>
                                    <div class="controls">
                                        <select id="staff" name="staff" data-rel="chosen">
                                            <option value="">Select Staff</option>
                                            <?php foreach($staffs as $staff):?>
                                                <option value="<?= $staff->id?>"><?= $staff->first_name .' '.$staff->last_name?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Status</label>
                                    <div class="controls">
                                    <label class="checkbox">
                                        <div class="checker disabled" id="uniform-optionsCheckbox2"><span><input type="checkbox" id="optionsCheckbox2" value="option1" disabled=""></span></div>
                                        This is a disabled checkbox
                                    </label>
                                        <!-- <label class="checkbox inline">
                                            <input type="checkbox" id="inlineCheckbox1" name="status" value="confirmed"> Confirm Quotation
                                        </label> -->
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Approve</label>
                                    <div class="controls">
                                    <label class="checkbox inline">
                                        <input type="checkbox" id="inlineCheckbox1" name="approved" value="1"> Approve Quotation
                                    </label>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Payment</label>
                                    <div class="controls">
                                    <label class="checkbox inline">
                                        <input type="checkbox" id="inlineCheckbox1" name="proceedToPayment" value="1"> Proceed to Payment ?
                                    </label>
                                    </div>
                                </div>

                                <div class="control-group hidden-phone">
                                    <label class="control-label" for="comment">Comment</label>
                                    <div class="controls">
                                        <textarea class="" id="comment" rows="3"></textarea>
                                    </div>
                                </div>

                            

							<div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save changes</button>
							</div>
							</fieldset>
						  </form>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

    
<?php temp_view('list',$quotations);?>
