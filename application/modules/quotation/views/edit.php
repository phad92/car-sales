<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Create Order</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="<?= base_url().'order/update/'.$id?>" method="post">
							<fieldset>

                            <!-- <div class="row"> -->
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Product</label>
                                    <div class="controls">
                                        <select id="product" name="product" data-rel="chosen">
                                            <option value="">Select Product</option>
                                            <?php foreach($products as $row):?>
                                                <option value="<?= $row->id?>" <?= selected_option($row->id,$product)?>><?= $row->name?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
									<label class="control-label" for="focusedInput">Quantity</label>
									<div class="controls">
										<input class="input-xlarge focused" name="quantity" value="<?= $quantity?>" id="focusedInput" type="number" min="1" value="1" placeholder="Phone">
									</div>
							  </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Client</label>
                                    <?php //pp($makes)?>
                                    <div class="controls">
                                        <select id="client" name="client" data-rel="chosen">
                                            <option value="">Select Client</option>
                                            <?php foreach($clients as $row):?>
                                                <option value="<?= $row->id?>" <?= selected_option($row->id, $client)?>><?= account_name('clients',$client)?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="focusedInput">Staff</label>
                                    <?php //pp($makes)?>
                                    <div class="controls">
                                        <select id="staff" name="staff" data-rel="chosen">
                                            <option value="">Select Staff</option>
                                            <?php foreach($staffs as $row):?>
                                                <option value="<?= $row->id?>" <?= selected_option($row->id, $staff)?>><?= account_name('staffs',$staff);?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Status</label>
                                    <div class="controls">
                                    <label class="checkbox inline">
                                        <input type="checkbox" id="inlineCheckbox1" name="status" value="confirmed" <?= checked_option($status, 'confirmed')?>> Confirm Order
                                    </label>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Approve</label>
                                    <div class="controls">
                                    <label class="checkbox inline">
                                        <input type="checkbox" id="inlineCheckbox1" name="approved" value="1" <?= checked_option($approved, 1) ?>> Approve Order
                                    </label>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Payment</label>
                                    <div class="controls">
                                    <label class="checkbox inline">
                                        <input type="checkbox" id="inlineCheckbox1" name="proceedToPayment" value="1"> Proceed to Payment ?
                                    </label>
                                    </div>
                                </div>

                                <div class="control-group hidden-phone">
                                    <label class="control-label" for="comment">Comment</label>
                                    <div class="controls">
                                        <textarea class="" id="comment" rows="3"></textarea>
                                    </div>
                                </div>

                            

							<div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save changes</button>
							</div>
							</fieldset>
						  </form>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

    
<?php temp_view('list',$orders);?>
