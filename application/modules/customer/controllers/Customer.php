<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Customer extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->load->model('auth/Mdl_auth');
        $this->model = new main_model;
        $this->model->setTable('users');
    }

    public function customers()
      { 
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['customers'] = fetch_accounts('clients');
          $data['headline'] = "List of Customers";
          $data['view_file'] = 'list';
          load_admin($data);
      }

      public function customer($update_id)
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['customer'] = $this->Mdl_auth->get_accounts_by_group('clients',$update_id);
          $data['headline'] = "Customer Detail";
          $data['view_file'] = "detail";
          load_admin($data);
      }


      public function add(){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $data['headline'] = "Add Customer";
          $data['message'] = get_flashdata();
          $data['customers'] = fetch_accounts('clients');
          $data['view_file'] = 'create';
          load_admin($data);
      }

      public function save()
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');
          $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
         
            if ($this->form_validation->run()) {
                $data = $this->_get_form_data();
                // pp($data);
              if (!$this->model->_insert($data)) {

                  $message = alert_message("Database operation failed", false);
                //   print_r($message);die();
                  set_flashdata($message);
                  redirect('customer/add');
              }else{
                  
                  
                  $client_group_id = $this->Mdl_auth->get_group_id_by_name('clients')->id;
                //   pp($client_group_id);

                  $client_id = $this->model->get_max();

                  $this->ion_auth->add_to_group($client_group_id, $client_id);
                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect('customer/add');
                }
                
            } else {
                $data['message'] = validation_errors('<div class="alert alert-danger">', '</div>');
                
                redirect('cutomer/add');
                
          }

      }

      public function edit(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

        $update_id = $this->uri->segment(3);
          
        $customer = $this->Mdl_auth->get_account_by_group('clients',$update_id);

        $data = $this->_get_db_data($customer);
        $data['headline'] = "Edit $customer->first_name";
        $data['customers'] = $this->Mdl_auth->get_accounts_by_group('clients');
        $data['message'] = get_flashdata();
        $data['id'] = $update_id;
        $data['view_file'] = 'edit';
        load_admin($data);
      }

      public function update($update_id)
      { 
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }

          $this->load->library('form_validation');
          $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
         
            if ($this->form_validation->run()) {
                $data = $this->_get_form_data();
              if (!$this->model->_update($update_id,$data)) {

                  $message = alert_message("Database operation failed", false);
                //   print_r($message);die();
                  set_flashdata($message);
                  redirect('customer/edit/'.$update_id);
              }else{

                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect('customer/add');
                }
                
            } else {
                $data['message'] = validation_errors('<div class="alert alert-danger">', '</div>');
                
                redirect('cutomer/edit/'.$update_id);
                
          }
      }

      public function delete($id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          if($this->model->_delete($id)){
              $message = alert_message('delete successful', false);
              set_flashdata($message);
              redirect('customer/customers');
          }
      }

      function _get_db_data($customer){
          //  pp($customer);
        // $data['username'] = $customer->username;
        $data['first_name'] = $customer->first_name;
        $data['last_name'] = $customer->last_name;
        $data['phone'] = $customer->phone;
        $data['email'] = $customer->email;
        $data['physical_address'] = $customer->physical_address;
        $data['city'] = $customer->city;
        $data['type_of_id'] = $customer->type_of_id;
        $data['id_number'] = $customer->id_number;
        $data['tin'] = $customer->tin;
        // pp($data);
        return $data;
      }

      function _get_form_data($update_id = null){
        $id_count = $this->model->get_max() + 1;
        $this->load->model('ion_auth_model','iam');
        $data['code'] = uniq_id();
        $data['username'] = 'customer_'.$id_count;
        $data['password'] = $this->iam->hash_password($data['username']);
        $data['first_name'] = input_post('first_name');
        $data['last_name'] = input_post('last_name');
        $data['phone'] = input_post('phone');
        $data['email'] = input_post('email');
        $data['physical_address'] = input_post('physical_address');
        $data['city'] = input_post('city');
        $data['type_of_id'] = input_post('type_of_id');
        $data['id_number'] = input_post('id_number');
        $data['tin'] = input_post('tin');
        $data['date_added'] = time();
        $data['ip_address'] = $this->input->ip_address();
        // pp($data);
        if(isset($update_id)){
            unset($data['username']);
            unset($data['password']);
            unset($data['code']);
            unset($data['date_added']);
        }

        return $data;
      }
}
