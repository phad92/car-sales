<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_service extends CI_Model {

    protected $table;

function __construct() {
parent::__construct();
$this->table = 'cs_service';
}

public function _service_details(){
    $query_string = "$this->table.id, $this->table.name, $this->table.price, CONCAT(employee.firstname, ' ' ,employee.lastname) AS employee";

    $this->db->select($query_string);
    $this->db->from($this->table);
    $this->db->join('employee',"$this->table.employee = employee.code");
    return $this->db->get()->result();
}
}
