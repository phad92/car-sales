<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

header("Access-Control-Allow-Origin: *");
class Service extends MX_Controller
{

    protected $model;

    function __construct() {
        parent::__construct();
        $this->model = new main_model;
        $this->model->setTable('cs_service');
    }

    public function services()
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['headline'] = "Services";
        $data['view_file'] = "list";
        $data['services'] = $this->model->get('name')->result();
        load_admin($data);
    }

    public function service($update_id)
    {
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['service'] = $this->model->get_where($update_id)->row_array();
        $data['id'] = $update_id;
        $this->load->model('mdl_service','pm');
        
        $data['services'] = $this->pm->_service_details();
        $data['headline'] = 'Service Detail';
        $data['view_file'] = 'detail';
        load_view($data);
    }

    public function add(){
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
        $data['services'] = $this->model->get('name')->result();
        $data['staffs'] = fetch_accounts('staffs');
        // pp($data['staffs']);
        $data['headline'] = 'Create Service';
        $data['message'] = get_flashdata();
        $data['view_file'] = 'create';
        load_admin($data);
    }

    public function save()
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          
          $this->load->library('form_validation');

          $this->form_validation->set_rules('name', 'Name', 'trim|required|is_unique[cs_service.name]');
        
        if ($this->form_validation->run()) {
            $data = $this->_get_form_data();

          if (!$this->model->_insert($data)) {
                  $message = alert_message("Database operation failed", false);
                  set_flashdata($message);
                  redirect('service/add');
              }else{
                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect('service/add');
              }
          } else {
              $message = validation_errors('<div class="alert alert-danger">', '</div>');
              set_flashdata($message);
                redirect('service/add');
          }

      }

      public function edit($update_id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->model('mdl_service');
          $service = $this->model->get_where($update_id)->row();
          $data = $this->_get_db_data($service);
          
          $data['services'] = $this->model->get('code')->result();
          $data['staffs'] = fetch_accounts('staffs');
          
          $data['headline'] = "Edit $service->name";
          $data['message'] = get_flashdata();
          $data['view_file'] = 'edit';
          load_admin($data);
      }

      public function update($update_id)
      {
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          $this->load->library('form_validation');

          $this->form_validation->set_rules('name', 'Name', 'trim|required');
          
          if ($this->form_validation->run()) {
                $data = $this->_get_form_data();

              if (!$this->model->_update($update_id,$data)) {

                  $message = alert_message("Database operation failed", false);
                  set_flashdata($message);
                  redirect('service/edit/'.$update_id);
              }else{

                  $message = alert_message("Success", true);
                  set_flashdata($message);
                  redirect('service/add');
                }
            } else {
                $message = validation_errors('<div class="alert alert-danger">', '</div>');
                set_flashdata($message);
                redirect('service/edit/'.$update_id); }
      }

      public function delete($id){
          if(!$this->ion_auth->logged_in()){
            redirect('auth/login');
          }
          if($this->model->_delete($id)){
              $message = alert_message('delete successful', true);
              set_flashdata($message);
                  redirect($this->url_segments.'/services');
          }
      }


    function _get_db_data($service){
        $data['id'] = $service->id;
        $data['name'] = $service->name;
        $data['price'] = $service->price;
        $data['staff'] = $service->staff;
        $data['timeframe'] = $service->timeframe;
        $data['comment'] = $service->comment;
        return $data;
      }

      function _get_form_data($update_id = null){
        
        $data['code'] = uniq_id();
        $data['name'] = input_post('name');
        $data['price'] = input_post('price');
        $data['staff'] = input_post('staff');
        $data['timeframe'] = input_post('timeframe');
        $data['comment'] = input_post('comment');
        $data['date_added'] = time();

        if(isset($update_id)){
            unset($data['code']);
            unset($data['unique_id']);
            unset($data['date_added']);
        }

        return $data;
      }


  
}
