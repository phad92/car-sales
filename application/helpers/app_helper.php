
<?php

if(!function_exists('alert_message')){
    function alert_message($message, $success= false)
    {
        $state = ($success) ? 'success' : 'danger';
        
        $skeleton = "<div class='alert alert-$state alert-dismissible fade in' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                        </button>
                        <strong>$message</strong>
                    </div>";
        return $skeleton;
    }
}

//This function generates random codes
if (!function_exists('uniq_id')) {
	function uniq_id($lenght = 13) {
		// uniqid gives 13 chars, but you could adjust it to your needs.
		if (function_exists("random_bytes")) {
			$bytes = random_bytes(ceil($lenght / 2));
		} elseif (function_exists("openssl_random_pseudo_bytes")) {
			$bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
		} else {
			throw new Exception("no cryptographically secure random function available");
		}
		return substr(bin2hex($bytes), 0, $lenght);
	}
	
	/*/Usage
	for($i=0;$i<20;$i++) {
		echo uniqid(), "\t", uniqidReal(), PHP_EOL;
	}
	*/
}

if(!function_exists('load_view')){
    function load_view($data){
        ci()->load->module('template');
        ci()->template->public_layout($data);
    }
}

if(!function_exists('load_auth')){
    function load_auth($data){
        ci()->load->module('template');
        ci()->template->auth_layout($data);
    }
}

if(!function_exists('load_admin')){
    function load_admin($data){
        ci()->load->module('template');
        ci()->template->admin_layout($data);
    }
}


if(!function_exists('load_assets')){
    function load_assets(){
        return base_url().'public/assets/';
    }
}

if(!function_exists('input_post')){
    function input_post($input_data){
        return ci()->input->post($input_data,true);
    }
}

if(!function_exists('get_post')){
    function get_post($key){
        if(array_key_exists($key,$_GET)){
            return $_GET[$key];
        }

        return null;
    }
}

if(!function_exists('temp_view')){
    function temp_view($view,$data){
        ci()->load->view($view,$data);
    }
}

if(!function_exists('set_flashdata')){
    function set_flashdata($value){
        ci()->session->set_flashdata('message', $value);
    }
}

if(!function_exists('get_flashdata')){
    function get_flashdata(){
        return ci()->session->flashdata('message');
    }
}


if(!function_exists('post_data')){
    function post_data(){
        $_POST = json_decode(file_get_content('php://input'));
        if($_POST){
          return $_POST;
        }
    }
}

if (!function_exists( 'pp' )) {
    function pp($dump)
    { 
        // echo '<pre>';
        // print_r($dump);
        // echo '</pre>';
        echo highlight_string("<?php\n\$data =\n" . var_export($dump, true) . ";\n//>");
        echo '<script>document.getElementsByTagName("code")[0].getElementsByTagName("span")[1].remove() ;document.getElementsByTagName("code")[0].getElementsByTagName("span")[document.getElementsByTagName("code")[0].getElementsByTagName("span").length - 1].remove() ; </script>';
        die();
    }
}


// Db Helpers
if(!function_exists('foreign_row')){
    function foreign_row($table,$id){
        $modelInstance = new main_model;
        $modelInstance->setTable($table);
        return $modelInstance->get_where($id)->row();
    }
}

if(!function_exists('foreign_result')){
    function foreign_result($table,$column){
        $modelInstance = new main_model;
        $modelInstance->setTable($table);
        return $modelInstance->get($column)->result();
    }
}

if(!function_exists('account_name')){
    function account_name($group_name,$account_id){
        ci()->load->model('auth/mdl_Auth','ma');
        $account = ci()->ma->get_account_by_group($group_name,$account_id);

        return ucwords($account->first_name .' '.$account->last_name);
    }
}

if(!function_exists('fetch_account')){
    function fetch_account($group_name,$account_id){
        ci()->load->model('auth/mdl_auth','ma');
        return ci()->ma->get_account_by_group($group_name,$account_id);
    }
}

if(!function_exists('fetch_accounts')){
    function fetch_accounts($group_name){
        ci()->load->model('auth/Mdl_auth','ma');
        return ci()->ma->get_accounts_by_group($group_name);
    }
}


if(!function_exists('get_current_tracking_status')){
    function get_current_tracking_status($order_id){
        $modelInstance = new main_model;
        $modelInstance->setTable('cs_track_order');
        $track_order = $modelInstance->get_where_custom('cs_order',$order_id);
        // pp($track_order);
        if($track_order->num_rows() < 1){
            return -1;
        }

        return $track_order->row()->current_status;
    }
}

if(!function_exists('readable_current_state')){
    function readable_current_state($current_state){
        ci()->load->config('app_config');
        $tracking_states = ci()->config->item('order_tracking');

        foreach($tracking_states as $index => $state){
            if($index == $current_state){
                return $state;
            }
        }
    }
}


if(!function_exists('selected_option')){
    function selected_option($option,$selected_option){
        if($option == $selected_option){
            $selected = 'selected';
        }else{
            $selected = null;
        }

        return $selected;
    }
}

if(!function_exists('checked_option')){
    function checked_option($option,$checked_option){
        if($option == $checked_option){
            $checked = 'checked';
        }else{
            $checked = null;
        }

        return $checked;
    }
}








