<?php defined('BASEPATH') OR exit('No direct script access allowed');

//Tracking Processes
$config['order_tracking'] = ['ready','on route', 'on sea', 'at port', 'clearing', 'arrived'];