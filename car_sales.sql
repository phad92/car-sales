-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2019 at 11:12 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `firstname` int(11) NOT NULL,
  `lastname` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `booking_date` int(11) NOT NULL,
  `booking_type` varchar(150) NOT NULL,
  `employee` varchar(150) NOT NULL,
  `service` varchar(150) NOT NULL,
  `booking` varchar(150) NOT NULL,
  `start_time` int(11) NOT NULL,
  `timeframe` int(11) NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `code`, `booking_date`, `booking_type`, `employee`, `service`, `booking`, `start_time`, `timeframe`, `date_added`) VALUES
(1, '2508855646f8e', 1547198107, 'premium', 'em_3NtxvkvY3l6bvBQCYrocLXm2', '940ea65829b44', 'f2e8e4776e9bb', 1547190458, 60, 1547200778);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `unique_id` varchar(150) NOT NULL,
  `status` varchar(50) NOT NULL,
  `customer` varchar(150) NOT NULL,
  `payment_method` varchar(150) NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `code`, `unique_id`, `status`, `customer`, `payment_method`, `date_added`) VALUES
(1, 'f2e8e4776e9bb', '5f938', 'pending', 'co_FSN4UYt8Qf9AOBLAP5poTjPr', 'cash', 1547199470);

-- --------------------------------------------------------

--
-- Table structure for table `cs_car_color`
--

CREATE TABLE `cs_car_color` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_category`
--

CREATE TABLE `cs_category` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_category`
--

INSERT INTO `cs_category` (`id`, `name`) VALUES
(1, 'SUV'),
(2, 'pick up'),
(3, 'four wheel drive');

-- --------------------------------------------------------

--
-- Table structure for table `cs_color`
--

CREATE TABLE `cs_color` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_color`
--

INSERT INTO `cs_color` (`id`, `name`) VALUES
(1, 'orange'),
(51, 'red'),
(52, 'blue'),
(53, 'pink');

-- --------------------------------------------------------

--
-- Table structure for table `cs_fuel_type`
--

CREATE TABLE `cs_fuel_type` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_fuel_type`
--

INSERT INTO `cs_fuel_type` (`id`, `name`) VALUES
(1, 'gasoline');

-- --------------------------------------------------------

--
-- Table structure for table `cs_make`
--

CREATE TABLE `cs_make` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_make`
--

INSERT INTO `cs_make` (`id`, `name`) VALUES
(1, 'corolla'),
(2, 'benz'),
(4, 'nissan'),
(5, 'lincon');

-- --------------------------------------------------------

--
-- Table structure for table `cs_model`
--

CREATE TABLE `cs_model` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `make` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_model`
--

INSERT INTO `cs_model` (`id`, `name`, `make`) VALUES
(4, 'c250', 2),
(5, 'lip gloss', 2),
(6, 'venza', 4);

-- --------------------------------------------------------

--
-- Table structure for table `cs_order`
--

CREATE TABLE `cs_order` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `staff` int(11) NOT NULL,
  `status` varchar(25) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `comment` text,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_order`
--

INSERT INTO `cs_order` (`id`, `code`, `product`, `quantity`, `client`, `staff`, `status`, `approved`, `approved_by`, `comment`, `date_added`) VALUES
(6, 'fccef3', 3, 1, 10, 13, 'confirmed', 1, NULL, NULL, 1549224225),
(7, '882798', 5, 1, 10, 14, 'confirmed', 1, NULL, NULL, 1549225583);

-- --------------------------------------------------------

--
-- Table structure for table `cs_payment`
--

CREATE TABLE `cs_payment` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `cs_order` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_status` tinyint(1) DEFAULT '0',
  `payment_method` varchar(150) NOT NULL,
  `cheque_num` int(20) DEFAULT NULL,
  `cheque_status` tinyint(1) DEFAULT NULL,
  `comment` text,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_payment`
--

INSERT INTO `cs_payment` (`id`, `code`, `cs_order`, `amount`, `payment_status`, `payment_method`, `cheque_num`, `cheque_status`, `comment`, `date_added`) VALUES
(4, '85ebb8', 7, '35000.00', 1, 'cheque', 2147483647, NULL, '', 1549556557),
(5, '266148', 7, '21000.00', 1, 'cash', 0, NULL, '', 1549556685);

-- --------------------------------------------------------

--
-- Table structure for table `cs_product`
--

CREATE TABLE `cs_product` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `shipping_cost` decimal(8,2) DEFAULT NULL,
  `make` int(11) DEFAULT NULL,
  `model` int(11) DEFAULT NULL,
  `model_year` int(11) DEFAULT NULL,
  `transmission` varchar(75) DEFAULT NULL,
  `car_image` varchar(255) DEFAULT NULL,
  `description` text,
  `date_added` int(11) DEFAULT NULL,
  `inland_transport_cost` decimal(8,2) NOT NULL,
  `inland_insurance` int(11) NOT NULL,
  `misc` text NOT NULL,
  `releases` varchar(255) NOT NULL,
  `estimated_duty` int(11) NOT NULL,
  `vin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_product`
--

INSERT INTO `cs_product` (`id`, `code`, `shipping_cost`, `make`, `model`, `model_year`, `transmission`, `car_image`, `description`, `date_added`, `inland_transport_cost`, `inland_insurance`, `misc`, `releases`, `estimated_duty`, `vin`) VALUES
(1, '39b9dfc719aee', NULL, 2, 5, 2013, NULL, NULL, NULL, 1554897060, '23500.00', 1452, 'ddf', '2dgdf', 221, 565),
(2, '01cdbe31eb253', '2500.00', 2, 5, 2013, NULL, NULL, NULL, 1554897210, '23500.00', 1452, 'ddf', '2dgdf', 221, 565);

-- --------------------------------------------------------

--
-- Table structure for table `cs_quotation`
--

CREATE TABLE `cs_quotation` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `client` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_added` int(11) NOT NULL,
  `date_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_service`
--

CREATE TABLE `cs_service` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(255) NOT NULL,
  `staff` varchar(150) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `timeframe` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_service`
--

INSERT INTO `cs_service` (`id`, `code`, `name`, `staff`, `price`, `timeframe`, `comment`, `date_added`) VALUES
(1, '253f47e4f163d', 'car wash', '13', '250.00', 60, 'fadlu haruna not around', 1549452953),
(2, '92f3db10228c9', 'Vulganizing', '14', '5.00', 15, '', 1549555777);

-- --------------------------------------------------------

--
-- Table structure for table `cs_tracking_state`
--

CREATE TABLE `cs_tracking_state` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_track_order`
--

CREATE TABLE `cs_track_order` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL DEFAULT '0',
  `cs_order` int(11) NOT NULL,
  `client` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `current_status` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_created` int(11) NOT NULL,
  `date_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cs_track_order`
--

INSERT INTO `cs_track_order` (`id`, `code`, `cs_order`, `client`, `created_by`, `current_status`, `comment`, `date_created`, `date_updated`) VALUES
(2, '44f4e71a', 7, 10, 14, 0, '', 1549225583, 0),
(3, '3953f821', 1, 8, 13, 1, '', 1549228961, 1549963172);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `firstname` varchar(155) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `code`, `firstname`, `lastname`, `email`, `phone`, `date_added`) VALUES
(4, 'co_FSN4UYt8Qf9AOBLAP5poTjPr', 'fadlu', 'haruna', 'fadlu.haruna1@gmail.com', '573400638', 1541883995),
(5, 'co_hAIHRd3Iho6izKuD3x5rXBE5', 'Gadafi', 'haruna', 'info@tarkwastore.com', '0245025021', 1541884258);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `firstname` varchar(155) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `date_added` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `code`, `firstname`, `lastname`, `email`, `phone`, `date_added`) VALUES
(1, 'em_flmimt8szDuTsKNM95xDZOq7', 'fadlu', 'haruna', 'f@gmail.com', '0240225867', 1542279154),
(2, 'em_3NtxvkvY3l6bvBQCYrocLXm2', 'owusu', 'mason', 'm.owusu@gmail.com', '0202254865', 1541898635);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'clients', 'Clients'),
(4, 'staffs', 'staffs');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `make` int(11) DEFAULT NULL,
  `model` int(11) DEFAULT NULL,
  `model_year` int(11) DEFAULT NULL,
  `transmission` varchar(75) DEFAULT NULL,
  `fuel_type` int(11) DEFAULT NULL,
  `horse_power` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `car_condition` varchar(50) DEFAULT NULL,
  `seats` int(11) DEFAULT NULL,
  `doors` int(11) DEFAULT NULL,
  `car_image` varchar(255) DEFAULT NULL,
  `description` text,
  `date_added` int(11) DEFAULT NULL,
  `mileage` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `code`, `name`, `price`, `color`, `make`, `model`, `model_year`, `transmission`, `fuel_type`, `horse_power`, `category`, `car_condition`, `seats`, `doors`, `car_image`, `description`, `date_added`, `mileage`) VALUES
(2, 'co_Eviq3Zf3yJ7uW6IbwGSRxknS', 'corolla se', '500.00', 51, 1, 4, 2013, 'automatic', 1, 300, 1, 'new', 4, 4, '', NULL, 1541869649, 0),
(3, 'a620a76fa752c', 'camry 2014', '20000.00', 0, 2, 5, 0, '', 0, 0, 0, '', 0, 0, '', '', 1541869449, 0),
(5, '4493358e8c06a', 'nissan', '45000.00', 0, 2, 4, 2015, 'manual', NULL, 854, NULL, NULL, 8, 4, NULL, NULL, 1548433700, 0),
(6, '4af06a9c5515f', 'nissan', '52200.00', 0, 2, 5, 2015, 'manual', NULL, 854, NULL, NULL, 8, 4, NULL, NULL, 1548433805, 0),
(7, '74242ec8b2160', 'Fadlu Haruna', '54500.00', 0, 1, 5, 2015, 'automatic', NULL, 854, NULL, NULL, 8, 4, NULL, NULL, 1548434249, 0),
(8, 'dd26ed2a896fd', 'Fadlu Haruna', '30.00', 3, 1, 4, 2015, 'manual', NULL, 854, NULL, NULL, 8, 4, NULL, NULL, 1548435544, 0),
(9, 'eaf68307c227d', 'New suv', '500.00', 1, 1, 6, 2018, 'manual', 1, 250, NULL, 'new', 8, 4, NULL, NULL, 1549554366, 0),
(10, 'b9c87234e7fdc', 'toyota suv', '500.00', 1, 1, 5, 2016, 'manual', 1, 250, NULL, 'used', 6, 4, NULL, NULL, 1549554447, 0),
(11, '42474ee3f65b7', 'nissan', '2500.00', 1, 2, 4, 2018, 'automatic', 1, 500, 2, 'new', 2, 4, NULL, 'lkjl', 1554890546, 250000);

-- --------------------------------------------------------

--
-- Table structure for table `staff_roles`
--

CREATE TABLE `staff_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_roles`
--

INSERT INTO `staff_roles` (`id`, `role`, `description`) VALUES
(1, 'manager', 'general manager');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(150) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `role` int(11) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `date_added` int(11) UNSIGNED DEFAULT NULL,
  `physical_address` text NOT NULL,
  `city` varchar(150) NOT NULL,
  `type_of_id` varchar(255) NOT NULL,
  `id_number` int(11) NOT NULL,
  `tin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `ip_address`, `username`, `password`, `first_name`, `last_name`, `phone`, `company`, `email`, `role`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `date_added`, `physical_address`, `city`, `type_of_id`, `id_number`, `tin`) VALUES
(1, '0', '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', 'Admin', 'istrator', '0', 'ADMIN', 'admin@admin.com', 0, '', '', NULL, NULL, 'djEHgmF4WOI05cALDQqKou', 1268889823, 1554889606, 1, NULL, '', '', '', 0, 0),
(8, '02cfd1f07f4f9', '::1', 'customer_16', '$2y$08$J9/y.bKSozRggI5veH9DJu08IJ3dyxiSj3iBbSJ5K/anykBlVsUPW', 'fadlu', 'haruna', '0573400638', 'naba', 'info@tarkwastore.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1548709565, '', '', '', 0, 0),
(10, '19180d8eb71db', '::1', 'customer_9', '$2y$08$YMbvxxsmVFjZh4tMm8uD6.EHd5t5FQF5ghiv0qhiCPTjKpFMFTsTO', 'fred', 'amugi', '0245025021', 'cac-gh', 'info@tarkwastfrore.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1548451488, '', '', '', 0, 0),
(13, '84a8fbc939c2c', '::1', 'staff_11', '$2y$08$qe4H3lzPm/J2S0NWHKHQSuYS8P36JOvYYMXpMSu9VBegE2xNpWCpS', 'dennis', 'ankoma', '0245025021', NULL, 'info@tarkwastore.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1548455300, '', '', '', 0, 0),
(14, 'a52a6dca234d1', '::1', 'staff_14', '$2y$08$tKSUDQjJQijMuptYWKxcUOnrKNiJrPCZiwZq/6Wbv/eHyX1ukPNyW', 'phad', 'Store', '0245025021', NULL, 'info@tarkwastore.com', 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1548455934, '', '', '', 0, 0),
(15, '8fa4b8bb2d309', '::1', 'staff_15', '$2y$08$YCyukEh5TB5BtftIPifb9ufXJ8XfDKsTwdYCW/C8MXVeMyqDYSxDe', 'Tarkwa', 'Store', '0245025021', NULL, 'info@tarkwastore.com', 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1548456157, '', '', '', 0, 0),
(28, '4f6e20f11623d', '::1', 'customer_28', '$2y$08$XWM4qHvrcAvCu.NdOIJzBuxP8SQW8ueVkFRxAtRh0ZVPb4Gjwamde', 'george', 'wilson', '0557795913', 'cocoaboard', 'fadlu.haruna@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1549552864, '', '', '', 0, 0),
(29, '8f488bab5e509', '::1', 'customer_30', '$2y$08$CwIFcVYjP4723.ScPwtBbOJy1TW8GbET2.I68wYctCCgduBu.SzdO', 'nana', 'kwame', '0244258947', 'nanasco', 'nana@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1549553086, '', '', '', 0, 0),
(30, '30c9666fe31b4', '::1', 'customer_30', '$2y$08$QiC6WyCTjYliINai1oMogOsPBwQ5ATyAkgfDRXHKdlxR1M/oUX3wC', 'nick', 'cannon', '02452225568', NULL, 'info@tarkwastore.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1554899270, 'tarkwa street', 'tarkwa', 'passport', 0, 2147483647),
(31, 'cc8ee00942d82', '::1', 'customer_31', '$2y$08$zYq4PVskVU14rjXAG2MKCesLzRZBPybgEXQ1aBXb1TeDgAE1rid72', 'fadlu', 'haruna', '0573400638', NULL, 'info@tarkwastore.com', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1554900643, 'accra', 'accra', 'passport', 0, 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(6, 8, 3),
(8, 10, 3),
(11, 13, 4),
(12, 14, 4),
(13, 15, 4),
(16, 28, 3),
(17, 29, 3),
(18, 30, 3),
(19, 31, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_car_color`
--
ALTER TABLE `cs_car_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_category`
--
ALTER TABLE `cs_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_color`
--
ALTER TABLE `cs_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_fuel_type`
--
ALTER TABLE `cs_fuel_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_make`
--
ALTER TABLE `cs_make`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_model`
--
ALTER TABLE `cs_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_order`
--
ALTER TABLE `cs_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_payment`
--
ALTER TABLE `cs_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_product`
--
ALTER TABLE `cs_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_quotation`
--
ALTER TABLE `cs_quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_service`
--
ALTER TABLE `cs_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_tracking_state`
--
ALTER TABLE `cs_tracking_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_track_order`
--
ALTER TABLE `cs_track_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_roles`
--
ALTER TABLE `staff_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cs_car_color`
--
ALTER TABLE `cs_car_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_category`
--
ALTER TABLE `cs_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cs_color`
--
ALTER TABLE `cs_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `cs_fuel_type`
--
ALTER TABLE `cs_fuel_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cs_make`
--
ALTER TABLE `cs_make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cs_model`
--
ALTER TABLE `cs_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cs_order`
--
ALTER TABLE `cs_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cs_payment`
--
ALTER TABLE `cs_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cs_product`
--
ALTER TABLE `cs_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cs_quotation`
--
ALTER TABLE `cs_quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_service`
--
ALTER TABLE `cs_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cs_tracking_state`
--
ALTER TABLE `cs_tracking_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cs_track_order`
--
ALTER TABLE `cs_track_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `staff_roles`
--
ALTER TABLE `staff_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
